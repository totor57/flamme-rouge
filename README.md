Projet flamme rouge de Morelière Victor et Jacob-Guizon Xavier

       A prendre note:
-Dans le package principale vous trouverez 2 main, un qui lance le jeu en mode graphique et l'autre en mode console. Le mode graphique n'est pas encore entièrement fini, il est là à titre de démonstration!

      En plus des fonctionnalités de bases et des consignes de bases voici les fonctionnalités rajoutées à notre jeu:
-Sauvegarder son jeu (a la fin de chaque tour) et pouvoir le charger
-Créer et enregistrer son circuit de facon à pouvoir le rejouer
-L'ajout de la crevaison de facon aléatoire
-Les étapes montagnes sont optionnels et généré de facon aléatoire pour plus de diversité dans les circuits
-La possibilité de lancer le jeu en mode graphique

voici le lien du git : https://bitbucket.org/totor57/flamme-rouge/src/master/

Bonne correction!
