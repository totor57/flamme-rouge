package principale;

import java.util.*;
import java.io.*;

import joueur.Interation;
import joueur.Joueur;

import circuit.Circuit;
import exception.FinDeJeuException;

/**
 * Classe principale pour jouer en mode console
 * 
 * @author totor & Blondin
 *
 */
public class Main {

	/**
	 * Main principale en mode console 
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Jeu jeuCourant = null;
		//on informe le joueur du debut et de ce qu'il doit faire
		String choix = Interation.main_debut_Jeu();
		//si les joueurs veulent commencer une nouvelle partie
		if(!choix.equals("1")){
			System.out.println("Souhaitez vous charger un circuit existant (1) ou en créer un (tapez n'importe quoi d'autre) ?");
			String choixCreation = sc.next();
			Circuit circuit = null; 
			//Si ils souhaitent utiliser un circuit existant
			if(choixCreation.equals("1")){		
				//On liste les circuits possiblers
				String path=new File("").getAbsolutePath();
				File depotCircuit = new File(path+"/src/circuitcarte/");
				System.out.println("Pour vous aider voici la liste des circuits existants :");
				String[] listeCircuit = depotCircuit.list();
				String listeFichiers ="";
				for(int i = 0;i<listeCircuit.length;i++){
					listeFichiers += listeCircuit[i]+"\n";
				}
				System.out.println(listeFichiers);
				System.out.println("Maintenant entrez le nom d'un circuit: ");
				String nomCircuit = sc.next();
				try{
					circuit = Circuit.setCircuit(nomCircuit);
				}
				catch(IOException e){}
				//Tant que le nom n'est pas bon on boucle
				while(circuit == null){
					try{
						System.out.println("Inconnu! entrez un autre nom de circuit présent dans nos fichiers!");
						nomCircuit = sc.next();
						circuit = Circuit.setCircuit(nomCircuit);
					}
					catch(IOException e){}
				}
			}
			//Si ils souhaitent créer un circuit
			else{
				circuit = Interation.main_creerCircuit();
			}
			//On récupère le nombre de joueurs
			int nbJ = Interation.main_entreJoueurs();
			//on crée les joueurs
			List<Joueur> listeJ = new ArrayList<Joueur>();
			for(int i = 0; i<nbJ;i++){
				//pour tout les joueurs on leur demande leur nom
				Interation.main_nomJoueur(i,listeJ,circuit);
			}
			//On demande si le joueur veut une étape de montage et on la génère si oui
			String n = Interation.circuit_montagne();
			if(n.equals("1")){
				circuit.rendreEtapeMontage();
			}
			//on crée le jeu
			jeuCourant = new Jeu(circuit,listeJ,false);
			jeuCourant.setPositionDepart();
			System.out.println(jeuCourant);
		}
		//Si les joueurs veulent charger une sauvegarde:
		else{
			//On liste les nom des sauvegardes
			System.out.println("Voici la liste des sauvegardes actuelles:");
			String path=new File("").getAbsolutePath();
			File depotSaves = new File(path+"/src/sauvegardes/");
			String[] listeSaves = depotSaves.list();
			String listeSauvegardes ="";
			for(int i = 0;i<listeSaves.length;i++){
				listeSauvegardes += "\n"+listeSaves[i];
			}
			listeSauvegardes+="\n";
			System.out.println(listeSauvegardes);
			//Tant que le nom de sauvegarde n'est pas un nom valide
			while(jeuCourant == null){
				String nomSave = Interation.main_chargerSauvegarde();
				try{
					jeuCourant = Jeu.charcherSauvegarde(nomSave);
				}
				catch(IOException e){
					System.out.println("Nom de sauvegarde incorrect!");
				}
			}
			System.out.println(jeuCourant);
		
		}
		
		//save est vrai tant qu'on ne sauvegarde pas
		boolean save = true;		
		//boucle principale
		while(save){
				// on pioche une carte
				jeuCourant.piocherCarte();
				
				//On lance la derniere phase qui peut lancer une exception de fin de jeu!
				try{
					//la phase mouvement va permettre de deplacer les cyclistes en fonction des choix des joueurs
					jeuCourant.phaseMouvement();
				}
				//si jamais un ou plusieurs cycliste gagne, une exception est lancé
				catch (FinDeJeuException e){
					//on affiche que quelqu'un gagne
					System.out.println(e.getMessage());
					break;
				}
				//la derniere phase permet de ...
				jeuCourant.dernierePhase();
				//Vérification du bon placé (en case de crevaison
				jeuCourant.verificationPlacage();
				//on affiche le toString du jeu
				System.out.println("Etat du jeu :");
				System.out.println(jeuCourant);
				//on save???
				String choixSave = Interation.main_faireSauvegarde();
				//si le choix de sauvegarde est 1
				if(choixSave.equals("1")){
					//on donne un nom de sauvegarde
					String nomSave = Interation.main_nomSauvegarde();
					//on sauvegarde la partie
					jeuCourant.sauvegarder(nomSave);
					//save devient false (on arrete la partie)
					save = false;
				}
		}
		//on finit
		Interation.main_finMain();
		//on ecrit le classement
		jeuCourant.ecrirerLeClassementDansFichier();
		sc.close();
	}

}
