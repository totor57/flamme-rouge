package principale;
import exception.*;
import java.util.*;

import joueur.*;

import circuit.*;
import cartes.*;
import exception.FinDeJeuException;

import graphique.Fenetre;

import java.io.*;

/**
 * Classe du jeu courant 
 * @author totor & blondin
 *
 */
public class Jeu implements Serializable{

	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Pioche des cartes fatigues rouleur (commune à tous les joueurs)
	 */
	private List<Carte> carteFatigueRouleur;
	/**
	 * Pioche des cartes fatigues sprinteur (commune à tous les joueurs)
	 */
	private List<Carte> carteFatigueSprinteur;
	/**
	 * Liste des joueurs dans la partie
	 */
	private List<Joueur> listeJ;
	/**
	 * Circuit de la partie
	 */
	private Circuit piste;
	/**
	 * Tableau mulidimensionel des cyclistes de la taille du nombre de cases du circuit et sur deux lignes pour droite et gauche(0=>cycliste à droite 1=>à gauche) 
	 */
	private Cycliste[][] tabPosCycliste;
	/**
	 * l'element fen permet de modifier graphiquement des elements du plateau
	 */
	private Fenetre fen;
	
	/**
	 * Vrai si on est en modeGraphique, faux sinon
	 */
	private boolean modeGrahpe;
	
	/**
	 * Constructeur d'une instance de jeu
	 * @param p circuit du Jeu
	 * @param l Liste des joueurs
	 */
	public Jeu(Circuit p,List<Joueur> l,boolean mode){
		this.modeGrahpe = mode;
		this.listeJ = l;
		this.piste=p;
		this.carteFatigueRouleur = new ArrayList<Carte>();
		this.carteFatigueSprinteur = new ArrayList<Carte>();
		for(int i = 0;i<30;i++){
			this.carteFatigueRouleur.add(new CarteFatigue());
			this.carteFatigueSprinteur.add(new CarteFatigue());
		}
		int nbCase = 0;
		for(Tuile tuileC : p.getListTuile()){
			nbCase+=tuileC.getNbCases();
		}
		this.tabPosCycliste = new Cycliste[nbCase+4][2];
	}
	
	
	/**
	 * Methode permettant de placer les cyclistes derriere la ligne de départ selon le choix des joueurs
	 */
	public void setPositionDepart(){
		Interation.jeu_PosDepartDebut();
		int[] save= new int[4];
		//pour tout les joueurs
		for(int i =0;i<listeJ.size();i++){
			//
			Interation.jeu_PosDepartMilieu(listeJ.get(i).getNom());
			//on recupere le choix de placement
			boolean valide = false;
			int choix = 0;
			while(!valide){
				try{
					choix = Interation.getNumber();
					if(choix <0 || choix >3){
						System.out.println("Le choix doit être entre 0 et 3!");
					}
					else{
						valide=true;
					}
				}
				catch(Exception e){
					System.out.println("Ce n'est pas un nombre ca! Rentrez un nouveau nombre");
					//debug
					Interation.debug();
				}
			}
			//on verifie si la place est libre
			while(!verifLibre(save,choix)){
				Interation.jeu_PosDepartBoucle();
				valide = false;
				while(!valide){
					try{
						choix = Interation.getNumber();
						if(choix <0 || choix >3){
							System.out.println("Le choix doit être entre 0 et 3!");
						}
						else{
							valide=true;
						}
					}
					catch(Exception e){
						System.out.println("Ce n'est pas un nombre ca!");
						Interation.debug();
					}
				}
			}
			//a chaque choix on incremente le tableau (pour savoir si il y a deja qqun
			save[choix]+=1;
			//on met la case du cycliste 0 du joueur courant a -choix
			listeJ.get(i).getListCycliste().get(0).setCaseTuile(-choix);
			
			//si le choix egale 2
			this.tabPosCycliste[3-choix][0] = listeJ.get(i).getListCycliste().get(0);
			if(save[choix]==2){
				listeJ.get(i).getListCycliste().get(0).setFileDroite(false);
				this.tabPosCycliste[3-choix][1] = listeJ.get(i).getListCycliste().get(0);
			}
			//ici on rend le premier cycliste choisit
			if(this!=null && this.modeGrahpe)
				fen.rendre(this);
			
			Interation.jeu_PosDepartMilieu2();
			choix = Interation.getNumber();
			while(!verifLibre(save,choix)){
				Interation.jeu_PosDepartBoucle();
				choix = Interation.getNumber();
			}
			save[choix]+=1;
			
			listeJ.get(i).getListCycliste().get(1).setCaseTuile(-choix);
			this.tabPosCycliste[3-choix][0] = listeJ.get(i).getListCycliste().get(1);
			if(save[choix]==2){
				listeJ.get(i).getListCycliste().get(1).setFileDroite(false);
				this.tabPosCycliste[3-choix][1] = listeJ.get(i).getListCycliste().get(1);
			}
			//ici on va demander aux graphique d'ajouter le deuxieme cycliste choisit au plateau en raffraichissant
			if(this!=null && this.modeGrahpe)
				fen.rendre(this);
		}
	}
	
	/**
	 * Permet de vérifier que la case n'est pas déjà occupé par 2 cyclistes
	 * @param backup tableau des poisitions des cyclistes sur la ligne de départ
	 * @param choix position voulue par le joueur
	 * @return vrai si la colonne peut accueillir un cycliste de plus
	 */
	private boolean verifLibre(int[] backup,int choix){
		boolean res = false;
		if(backup[choix]<2){
			res = true;
		}
		return res;
	}
	
	/**
	 * Fait piocher au joueur 3 cartes en mode console
	 */
	public void piocherCarte(){
		for(int i = 0; i <this.getListeJ().size();i++){
			Interation.jeu_Piocher(listeJ.get(i).getNom() );
			listeJ.get(i).piocher();
		}
	}
	
	/**
	 * Permet de trouver un joueur a partir de sa couleur
	 * @param couleur du joueur qu'on cherche
	 * @return joueur ayant cette couleur
	 */
	public Joueur trouverJoueur(String couleur){
		Joueur res = null;
		for(Joueur j : this.listeJ){
			if(j.getCouleur().equals(couleur)){
				res = j;
				break;
			}
		}
		
		return res;
	}
	
	/**
	 * Phase durant laquel les joueurs piochent et les cyclistes avancent en conséquence
	 * @throws FinDeJeuException exception en cas de dépassement de la ligne par un cycliste
	 */
	public void phaseMouvement()throws FinDeJeuException{
		//On crée une liste de cyclistes où on va tous les mettres
		List<Cycliste> listeCyc = new ArrayList<Cycliste>(); 
		for(Joueur j : this.listeJ){
			listeCyc.addAll(j.getListCycliste());
		}
		//on les trie (à l'indice 0 on aura celui qui a parcouru le plus de cases puis le 2eme etc..
		Collections.sort(listeCyc,new ComparatorCycliste());
		//ON parcourt la liste
		for(Cycliste cycCour : listeCyc){
			//on recupère le joueur de ce cyliste
			Joueur jc = this.trouverJoueur(cycCour.getCouleur());
			//nombre random pour savoir si le cycliste va crever!
			int n = (int)(Math.random()*100);
			//Si il n'a pas crevé
			if(n%20!=0){
				//on regarde si c'est le sprinteur ou le rouleur du joueur et on prend la carte en fonction
				if(cycCour instanceof Sprinter){
					cycCour.fairemouvement(jc.getCarteAjouer().get(new Integer(1)));
				}
				else{
					cycCour.fairemouvement(jc.getCarteAjouer().get(new Integer(2)));
				}
			}
			else{
				System.out.println(cycCour +", a crevé! Il n'a donc pas pu avancer durant ce tour!!");
			}
			//on rafraichit la fenetre si on est en mode graphique
			if(this.modeGrahpe) this.fen.rendre(this);	
		}
		
		//A la fin de la phase de mouvement on vérifie qu'aucun cycliste n'a gagné la course
		if(this.unJoueuraWin().size() != 0){
			//Si seuelement un seul a passé la ligne
			if(this.unJoueuraWin().size() == 1){
					throw new FinDeJeuException("C'est " + this.unJoueuraWin().get(0)  +" qui a gagné!");
			}
			//Si plusieurs ont passé la ligne
			else{
				List<Cycliste> temp = this.classementCycliste();
				//si les deux cyclistes les plus loins sont à égalité
				if(temp.get(0).getCaseTuile() == temp.get(1).getCaseTuile()){
					Cycliste win = Interation.departager_deuxCyclistes(temp.get(0),temp.get(1));
					throw new FinDeJeuException("C'est" + win +" qui a gagné!");
				}
				else{
					throw new FinDeJeuException("C'est" + temp.get(0) +" qui a gagné!");
				}
			}
		}
	}
	
	/**
	 *Permet de verifier que tous les cyclistes sur la file de gauche ont bien un cycliste sur leur droite 
	 */
	public void verificationPlacage(){
		for(int i = 0; i<this.tabPosCycliste.length;i++){
			if(this.tabPosCycliste[i][1] != null && this.tabPosCycliste[i][0] == null){
				this.tabPosCycliste[i][1].setFileDroite(true);
				this.tabPosCycliste[i][0] = this.tabPosCycliste[i][1];
				this.tabPosCycliste[i][1] = null;
			}
		}
	}
	
	/**
	 * Permet d'ajouter dans un fichier à la racine un fichier contenant le classement de la partie actuel
	 */
	public void ecrirerLeClassementDansFichier(){
		try{
			PrintWriter fichier = new PrintWriter(new FileWriter("dernierClassement"));
			List<Cycliste> ls = this.classementCycliste();
			int indice =1;
			int ancienneCase=-10;
			for(Cycliste cCourant : ls){
				if(cCourant.getCasePasse() == ancienneCase){
					fichier.println("A la " + (indice-1) + " place : "+ cCourant.toString());
				}
				else{
					fichier.println("A la " + indice + " place : "+ cCourant.toString());
				}
				ancienneCase=cCourant.getCasePasse();
				indice++;
			}
			fichier.close();
		}
		catch(IOException e){
			System.out.println("Oups! Il s'emblerait qu'il y est eu un soucis dans l'ouverture du fichier pour le classement.. Veuillez contacter les admins!");
		}
	}
	
	/**
	 * Cree une liste constitué des cyclistes ayant dépassé la ligne!
	 * @return Liste des cyclistes qui ont passé la ligne
	 */
	public List<Cycliste> unJoueuraWin(){
		List<Cycliste> res = new ArrayList<Cycliste>();
		for(Joueur j:this.listeJ){
			for(int i = 0; i <2;i++){
				if(j.getListCycliste().get(i).aPasseLaLigne()){
					res.add(j.getListCycliste().get(i));
				}
			}
		}
		return res;
	}
	
	/**
	 * Phase durant laquel l'aspiration est appliqué et les cartes fatigues attribués
	 */
	public void dernierePhase(){
		this.tabPosCycliste = new Cycliste[this.tabPosCycliste.length][2];
		//On parcourt les joueurs du jeu
		for(Joueur jc : this.listeJ){
			//On paarcout les cyclistes de chaque joueurs
			for(Cycliste cycCour : jc.getListCycliste()){
				//Si ils sont sur la grille de départ
				if(cycCour.getCasePasse() <= 0){
					if(cycCour.isFileDroite()){
						this.tabPosCycliste[cycCour.getCasePasse()+3][0]=cycCour;
					}
					else{
						this.tabPosCycliste[cycCour.getCasePasse()+3][1]=cycCour;
					}
					
				}
				//sinon on lui donne sa nouvelle place dans le jeu
				else if(cycCour.isFileDroite()){
					this.tabPosCycliste[cycCour.getCasePasse()+3][0] = cycCour;
				}
				else{
					this.tabPosCycliste[cycCour.getCasePasse()+3][1] = cycCour;
				}
			}
		}
		boolean aspirationFini = false;
		int indice =0;
		//tant qu'on n'a pas fini de faire l'aspiration
		while(!aspirationFini){
			//si on arrive au bout du tableau
			if(indice == this.tabPosCycliste.length){
				aspirationFini = true;
			}
			//si il n'y a pas de cyclistes on incrémente l'indice
			else if(this.tabPosCycliste[indice][0] == null){
				indice++;
			}
			//Si il y a un cycliste
			else{
				//Si sa case après est nul et celle d'après a un cycliste on le fait avancer
				if(this.tabPosCycliste[indice+1][0] == null && this.tabPosCycliste[indice+2][0] != null){
					this.tabPosCycliste[indice][0].subitAspiration();
					this.tabPosCycliste[indice+1][0] = this.tabPosCycliste[indice][0];
					this.tabPosCycliste[indice][0] =null;
					//Si il y avait un cycliste à côté du cycliste il recoit aussi l'aspiration
					if(this.tabPosCycliste[indice][1]!=null){
						this.tabPosCycliste[indice][1].subitAspiration();
						this.tabPosCycliste[indice+1][1] = this.tabPosCycliste[indice][1];
						this.tabPosCycliste[indice][1] =null;
					}
					//ON fait de même avec ceux qui pourraient être potentiellement derriere (groupe)
					for(int i = 1;i<3;i++){
						if(this.tabPosCycliste[indice-i][0] != null){
							this.tabPosCycliste[indice-i][0].subitAspiration();
							this.tabPosCycliste[indice-i+1][0] = this.tabPosCycliste[indice-i][0];
							this.tabPosCycliste[indice-i][0] =null;
							if(this.tabPosCycliste[indice-i][1]!=null){
								this.tabPosCycliste[indice-i][1].subitAspiration();
								this.tabPosCycliste[indice+1-i][0] = this.tabPosCycliste[indice][0];
								this.tabPosCycliste[indice-i][0] =null;
							}
						}
						//Dès qu'il n'y a personne on sort 
						else{
							break;
						}
					}
				}
				indice++;
			}
		}
		//On parcourt la liste des cyclistes et on leur applique un malus s'ils sont en tete de peltton
		for(int i = 0;i<this.tabPosCycliste.length;i++){
			//Si il y a un cycliste
			if(this.tabPosCycliste[i][0] != null){
				//Et que perssone n'est devant lui, il recoit malus
				if(this.tabPosCycliste[i+1][0] == null){
					Cycliste cyc = this.tabPosCycliste[i][0];
					Joueur jCyc = joueurCorrespondant(cyc.getCouleur());
					if(cyc instanceof Sprinter){
						jCyc.recoitMalusS(this.carteFatigueSprinteur.get(this.carteFatigueSprinteur.size()-1));
						this.carteFatigueSprinteur.remove(this.carteFatigueSprinteur.size()-1);
					}
					else{
						jCyc.recoitMalusR(this.carteFatigueRouleur.get(this.carteFatigueRouleur.size()-1));
						this.carteFatigueRouleur.remove(this.carteFatigueRouleur.size()-1);
					}
				}	
			}
		}
	}
	
	/**
	 * Permet de sauvegarder le jeu en cours
	 * @param nomSave nom du fichier de sauvegarde
	 */
	public void sauvegarder(String nomSave){
		try{
			String path=new File("").getAbsolutePath();
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path+"/src/sauvegardes/"+nomSave));
			oos.writeObject(this);
			oos.close();
		}
		catch(IOException e){
			System.out.println("Problème de sauvegarde!!");
		}
	}
	
	/**
	 * Permet de charger un jeu à partir du nom de sa sauvegarde
	 * @param nomSave nom de la sauvegarde du jeu
	 * @return jeu correspondant au nom de la sauvegarde
	 * @throws IOException envoye si le fichier est introuvable
	 */
	public static Jeu charcherSauvegarde(String nomSave)throws IOException{
		Jeu res = null;
		try{
			String path=new File("").getAbsolutePath();
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(path+"/src/sauvegardes/"+nomSave));
			res = (Jeu)(ois.readObject());
			ois.close();
		}
		catch(IOException e){
			throw new IOException();
		}
		catch(ClassNotFoundException e){
			System.out.println("Il semblerait que le type d'objet sauvegardé ne correpondent pas..");
		}
		return res;
	}
		
	/**
	 * Permet d'associer à une une couleur un joueur
	 * @param n couleur du joueur recherché
	 * @return le joueur correspondant à la couleur, si nous trouvé renvoie null
	 */
	public Joueur joueurCorrespondant(String n){
		Joueur res = null;
		for(Joueur jc : this.listeJ){
			if(jc.getCouleur().equals(n)){
				res = jc;
			}
		}
		return res;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		String res="";
		res+="n  ||D|G|| \n";
		for(int i = 0;i<this.tabPosCycliste.length;i++){
			if(i==4){
				res+="--------------\n";
			}
			if(i==this.tabPosCycliste.length-5){
				res+="--------------\n";
			}
			if((i+1)<10){
				res += (i+1) +"  ";
			}
			else if((i+1 <100)){
				res += (i+1) +" ";
			}
			else{
				res +=i;
			}
			res+="||";
			for(int j = 0;j<2;j++){
				if(j==1){
					res+="|";
				}
				if(this.tabPosCycliste[i][j]==null){
					res+="-";
				}
				else{
					res+="O";
				}
			}
			res+="|| \n";
		}
		return res;
	}
	
	/**
	 * Permet de trier les cyclistes en fonction du nombre de cases sur lesquelles ils se sont deplace 
	 * @return liste trie des cyclistes
	 */
	public List<Cycliste> classementCycliste(){
		List<Cycliste> res = new ArrayList<Cycliste>();
		for(Joueur jCourant : this.listeJ){
			res.addAll(jCourant.getListCycliste());
		}
		Collections.sort(res,new ComparatorCycliste());
		return res;
	}

	/**
	 * Permet d'obtenir la liste des cartes fatigues pour les rouleurs
	 * @return listes des cartes fatigues
	 */
	public List<Carte> getCarteFatigueRouleur() {
		return carteFatigueRouleur;
	}

	/**
	 * Permet de modifier la liste de carte fatigue rouleur actuel
	 * @param carteFatigueRouleur nouvelle liste de cartes fatigue rouleur
	 */
	public void setCarteFatigueRouleur(List<Carte> carteFatigueRouleur) {
		this.carteFatigueRouleur = carteFatigueRouleur;
	}

	/**
	 * Permet d'obtenir la liste des cartes fatigues pour les sprinteur
	 * @return listes des cartes fatigues
	 */
	public List<Carte> getCarteFatigueSprinteur() {
		return carteFatigueSprinteur;
	}

	/**
	 * Permet de modifier la liste de carte fatigue sprinteur actuel
	 * @param carteFatigue nouvelle liste de cartes fatigue sprinteur
	 */
	public void setCarteFatigueSprinteur(List<Carte> carteFatigue) {
		this.carteFatigueSprinteur = carteFatigue;
	}

	/**
	 * Permet d'obtenir la liste des joueurs sur le jeu courant
	 * @return liste des joueurs
	 */
	public List<Joueur> getListeJ() {
		return this.listeJ;
	}

	/**
	 * Permet de modifier la liste des joueurs sur le jeu courant
	 * @param listeJ nouvelle liste des joueurs
	 */
	public void setListeJ(List<Joueur> listeJ) {
		this.listeJ = listeJ;
	}

	/**
	 * Getter du circuit du jeu courant
	 * @return circuit du jeu
	 */
	public Circuit getPiste() {
		return piste;
	}

	/**
	 * Permet de modifier le circuit du jeu courant
	 * @param piste nouveau circuit
	 */
	public void setPiste(Circuit piste) {
		this.piste = piste;
	}

	/**
	 * Permet d'avoir la representation par tableau des cyclistes sur le jeu courrant
	 * @return tableau des cyclistes sur le tableau
	 */
	public Cycliste[][] getTabPosCycliste() {
		return tabPosCycliste;
	}

	/**
	 * Permet de modifier la representation par tableau des cyclistes sur le jeu courrant
	 * @param tabPosCycliste  nouveau tableau des cyclistes sur le tableau
	 */
	public void setTabPosCycliste(Cycliste[][] tabPosCycliste) {
		this.tabPosCycliste = tabPosCycliste;
	}

	/**
	 * cette methode permet de mettre la fenetre dans la classe Jeu (propre a l'affichage graphique)
	 * @param fen fenetre de l'interface graphique
	 */
	public void setFenetre(Fenetre fen) {
		this.fen=fen;
		
	}
	
	
}
