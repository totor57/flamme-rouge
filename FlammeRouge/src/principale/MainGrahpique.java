package principale;
import java.util.*;
import java.awt.Color;
import java.io.IOException;

import joueur.Interation;
import joueur.Joueur;
import circuit.Circuit;
import exception.FinDeJeuException;
import graphique.Fenetre;
import graphique.Plateau;

/**
 * Main pour lancer le jeu en mode graphique
 * @author totor & blondin
 *
 */
public class MainGrahpique {
	
	/**
	 * fenetre du graphique
	 */
	static Fenetre fen;
	
	/**
	 * Jeu du monde graphique 
	 */
	static Jeu jc;
	
	/**
	 * methode principale pour lancer le jeu
	 * @param args arguments paramétré
	 */
	public static void main(String[] args) {
		fen=new Fenetre();
		fen.setMenu();
		while(!Fenetre.getlancerJeu()){
			fen.repaint();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		MainGrahpique.setPosition();
		while(!Fenetre.getlancerJeu()){
			fen.repaint();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		MainGrahpique.lancerJeu(jc);
	}
	
	/**
	 * Permet de generer un jeu a partir des arguments mis en parametres
	 * @param nomCircuit nom du circuit
	 * @param montagne vrai si étap montagne faux sinon
	 * @param nomJ tableau
	 */
	public static void genererJeu(String nomCircuit,boolean montagne,String[] nomJ){
		Circuit circuit = null;
		try{
			circuit = Circuit.setCircuit(nomCircuit);
		}
		catch(IOException e){
			System.out.println("Probleme dans le nom!");
		}
		if(montagne){
			circuit.rendreEtapeMontage();
		}
		List<Joueur> lj = new ArrayList<Joueur>();
		String[] couleur = {"bleu","rouge","vert","jaune"};
		Color[] couleurG = {Color.blue,Color.red,Color.green,Color.yellow};
		int i = 0;
		for(String n : nomJ){
			Joueur temp = new Joueur(n,couleur[i],couleurG[i],circuit);
			i++;
			lj.add(temp);
		}
		Jeu jcour = new Jeu(circuit,lj,true);
		MainGrahpique.jc = jcour;
		Fenetre.setlancerJeu();
	}
	
	
	/**
	 * Permet de lancer le menu 
	 */
	public static void lancerMenu(){
		fen.setMenu();
	}
	
	/**
	 * Permet d'obtenir le jeu
	 * @return jeu du mode graphique
	 */
	public static Jeu getJeu(){
		return jc;
	}
	
	/**
	 * Permet de positionner les positions 
	 */
	public static void setPosition(){
		fen.dispose();
		fen=new Fenetre();
		fen.setPosition();
	}
	
	/**
	 * Permet de lancer le plateau avec le jeu courant
	 * @param j jeu courant
	 */
	public static void lancerJeu(Jeu j){
		fen.dispose();
		fen=new Fenetre();
		fen.setJeu(j);
		fen.setPlateau();
		fen.rendre(j);
		j.setFenetre(fen);
		MainGrahpique.boucle(j);
	}
	
	/**
	 * Lance la boucle général
	 * @param jeuCourant jeu avec laquelle on lancer
	 */
	public static void boucle(Jeu jeuCourant){
		boolean save = true;		
		//save est vrai tant qu'on ne sauvegarde pas
		while(save){
				fen.repaint();
				// on pioche une carte
				jc.piocherCarte();
				//On lance la phase mouvement qui peut lancer une exception de fin de jeu!
				try{
					//la phase mouvement va permettre de deplacer les cyclistes en fonction des choix des joueurs
					jeuCourant.phaseMouvement();
				}
				//si jamais un ou plusieurs cycliste gagne, une exception est lancé
				catch (FinDeJeuException e){
					//on affiche que quelqu'un gagne
					System.out.println(e.getMessage());
					break;
				}
				//la derniere phase permet de ...
				jeuCourant.dernierePhase();
				//on affiche le toString du jeu
				System.out.println("Etat du jeu :");
				System.out.println(jeuCourant);
				//on save???
				String choixSave = Interation.main_faireSauvegarde();
				//si le choix de sauvegarde est 1
				if(choixSave.equals("1")){
					//on donne un nom de sauvegarde
					String nomSave = Interation.main_nomSauvegarde();
					//on sauvegarde la partie
					jeuCourant.sauvegarder(nomSave);
					//save devient false (on arrete la partie)
					save = false;
				}
		}
		//on finit
		Interation.main_finMain();
		//on ecrit le classement
		jeuCourant.ecrirerLeClassementDansFichier();
	}

}
