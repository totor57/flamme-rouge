package circuit;

import java.io.Serializable;


/**
 * classe representant une tuile
 * @author totor & blondin 
 *
 */
public class Tuile implements Serializable{
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Nombre de cases de la tuile
	 */
	private int nbCases;
	/**
	 * Tableau des cases occupees (nombre de cyclistes sur chaque cases)
	 */
	private int[] casesOccupés;
	/**
	 *Vrai si la tuile est une descente 
	 */
	private boolean descente;
	/**
	 * Vrai si la tuile est une montee
	 */
	private boolean monte;
	
	/**
	 * Constructeur d'une tuile
	 * @param nbCases nombre de case de la tuile
	 */
	public Tuile(int nbCases) {
		this.nbCases = nbCases;
		this.casesOccupés = new int[nbCases];
		this.descente=false;
		this.monte=false;
	}
	
	/**
	 * Methode pour savoir si la tuile est une montee 
	 * @return vrai si la tuile est une montée
	 */
	public boolean estMonte(){
		return this.monte;
	}

	/**
	 * Permet de motifier le fait qu'une tuile soit une descente ou non
	 * @param descente vrai si la tuile doit etre une descente faut sinon
	 */
	public void setDescente(boolean descente) {
		this.descente = descente;
	}

	/**
	 * Permet de motifier le fait qu'une tuile soit une montee ou non
	 * @param monte vrai si la tuile doit etre une montée faut sinon
	 */
	public void setMonte(boolean monte) {
		this.monte = monte;
	}

	/**
	 * Methode pour savoir si une tuile est une descente ou non
	 * @return vrai si la tuile est une descente
	 */
	public boolean estDescente(){
		return this.descente;
	}
	

	/**
	 * Permet de savoir le nombre de cases d'une tuile
	 * @return le nombre de cases de la tuile courante
	 */
	public int getNbCases() {
		return nbCases;
	}
	
	/**
	 * Permet de savoir le nombre de cyclistes sur chaque case de la tuile
	 * @return tableau representant le nombre de cyclistes par rapport à la case
	 */
	public int[] getCasesOccues(){
		return this.casesOccupés;
	}
	
	
	/**
	 * Permet d'enlever un cycliste sur une case
	 * @param nbCases numéro de la cases de laquelle le cycliste part
	 */
	public void partCases(int nbCases){
		this.casesOccupés[nbCases-1]--;
	}

	/**
	 * Permet de modifier le nombre de cases d'une tuile
	 * @param nbCases nouveau nombre de cases de la tuile
	 */
	public void setNbCases(int nbCases) {
		this.nbCases = nbCases;
	}
	
	/**
	 * Permet d'ajouter un cycliste sur la case
	 * @param n numéro de la case sur laquelle le cycliste 
	 * @return la nouvelle valeur sur la case (nombre de cyclistes
	 */
	public int setCase(int n){
		this.casesOccupés[n-1]++;
		return (this.casesOccupés[n-1]);
	}
	
	
	/**
	 * Permet de verifier qu'une case n'est pas occupée par déjà 2 cyclistes
	 * @param n numéro de la case
	 * @return vrai si la case n'est pas déjà occupée par 2 cyclistes
	 */
	public boolean peutAller(int n){
		return(this.casesOccupés[n-1]<2);
	}

	
	
}
