package circuit;

import java.io.Serializable;

/**
 * Classe representant une tuile virage
 * @author totor & blondin
 *
 */
public class TuileVirage extends Tuile implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Type du virage (serre ou leger)
	 */
	String typeVirage;
	
	/**
	 * Permet de savoir la direction d'une tuile (utilisé dans la représentation graphique)
	 */
	boolean direction;
	
	/**
	 * Constructeur d'une tuile virage avec une direction (graphique), avec une taille fixe de 4
	 * @param type type de virage de la tuile
	 * @param direction direction de la tuile
	 */
	public TuileVirage(String type,boolean direction) {
		super(4);
		this.typeVirage = type;
		this.direction = direction;
	}
	
	/**
	 * Constructeur d'une tuile virage sans direction avec une taille non fixe
	 * @param nbCases nombre de cases d'une tuile
	 * @param type type de virage de la tuile
	 */
	public TuileVirage(int nbCases,String type) {
		super(nbCases);
		this.typeVirage = type;
	}
	
	/**
	 * permet de savoir si un virage est léger
	 * @return vrai si la tuile est un virage serre
	 */
	public boolean estLege(){
		return this.typeVirage.equals("leger");
	}
	
	/**
	 * permet de savoir si un virage est serre
	 * @return vrai si la tuile est un virage serre
	 */
	public boolean estSerre(){
		return this.typeVirage.equals("serre");
	}
	
	/**
	 * cette methode retourne la direction d'une tuile
	 * @return direction de la tuile
	 */
	public boolean getDirection() {
		return this.direction;
	}
}
