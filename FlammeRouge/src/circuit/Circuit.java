package circuit;

import java.util.*;
import java.io.*;



/**
 * Classe representant un circuit
 * @author totor
 *
 */
public class Circuit implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Nom du circuit
	 */
	private String nomCircuit;
	/**
	 * Liste des tuiles qui composent le circuit
	 */
	private List<Tuile> tabTuile;
	
	/**
	 * Est vrai si l'etape est de type montagne
	 */
	public boolean estEtapeMontage;
	
	/**
	 * Constructeur de circuit
	 * @param n nom du circuit 
	 * @param compo liste des tuiles qui composent le circuit
	 */
	public Circuit(String n,List<Tuile> compo){
		this.nomCircuit = n;
		this.tabTuile = compo;
		this.estEtapeMontage = false;	
	}
	
	/**
	 * Getter du nom du circuit
	 * @return le nom du circuit
	 */
	public String getNomCircuit(){
		return this.nomCircuit;
	}
	
	/**
	 * Getter de la liste des tuiles composants le circuit
	 * @return liste des tuiles
	 */
	public List<Tuile> getListTuile(){
		return this.tabTuile;
	}
	
	/**
	 * retourne la tuile a une position n de la liste
	 * @param n place dans la liste
	 * @return tuile qui est à la position n
	 */
	public Tuile tuileALaPosition(int n){
		return tabTuile.get(n);
	}
	
	
	/** Cree un circuit à partir d'un nom
	 * @param source nom du circuit voulu
	 * @return circuit cree
	 * @throws IOException exception si le nom du circuit n'est pas trouve
	 */
	public static Circuit setCircuit(String source)throws IOException{
		ArrayList<Tuile> composition = new ArrayList<Tuile>();
		try{
			int temp;
			//on recupere le path jusqu'au fichier
			String path=new File("").getAbsolutePath();
			BufferedReader circuit = new BufferedReader(new FileReader(path+"/src/circuitcarte/"+source));
			while((temp = circuit.read()) != -1){
				switch((char)temp){
				//depart
				case '0':
					composition.add(new TuileLigneDroite(true,false));
					break;
				//ligne droite normal
				case '1':
					composition.add(new TuileLigneDroite(false,false));
					break;
				//virage serré droit
				case '2':
					composition.add(new TuileVirage("serre",true));
					break;
				//virage léger droit
				case '3':
					composition.add(new TuileVirage("leger",true));
					break;
				//Arrivée
				case '4':
					composition.add(new TuileLigneDroite(false,true));
					break;
				//serré gauche
				case '5':
					composition.add(new TuileVirage("serre",false));
					break;
				//leger gauche
				case '6':
					composition.add(new TuileVirage("leger",false));
					break;
				default:
					System.out.println("Case inconnu");
					break;
				}	
			}
			circuit.close();
		}
		catch(IOException e){
			throw new IOException();
		}
		Circuit c = new Circuit(source,composition);
		return c;
		
	}
	
	/**
	 * Permet de faire que le circuit courant soit une étape de montagne
	 */
	public void rendreEtapeMontage(){
		this.estEtapeMontage=true;
		for(Tuile c : this.tabTuile){
		int n = (int)(Math.random()*100);	
			if(n%3==0){
				c.setDescente(true);
				c.setMonte(false);
			}
			else if(n%2 == 0){
				c.setMonte(true);
				c.setDescente(false);
			}
			else{
				c.setMonte(false);
				c.setDescente(false);
			}
		}
	}
	
	
	/**
	 * Getter de la liste de tuile du circuit
	 * @return la liste de tuile du circuit
	 */
	public List<Tuile> getTabTuile() {
		return tabTuile;
	}

	/**
	 * Permet de modifier la liste de tuile d'un circuit (surtout utilise pour tests)
	 * @param tabTuile list de tuile qu'on veut donner au circuit courant
	 */
	public void setTabTuile(List<Tuile> tabTuile) {
		this.tabTuile = tabTuile;
	}

	/**
	 * Modifier le nom du circuit
	 * @param nomCircuit nouveau nom du circuit
	 */
	public void setNomCircuit(String nomCircuit) {
		this.nomCircuit = nomCircuit;
	}
	
	

}
