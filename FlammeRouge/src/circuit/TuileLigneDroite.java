package circuit;

import java.io.Serializable;


/**
 * Classe representant une ligne droite
 * @author totor & blondin
 *
 */
public class TuileLigneDroite extends Tuile implements Serializable{
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Attribut vrai si la tuile est le depart
	 */
	private boolean estDepart;
	/**
	 * Attribut vrai si la tuile est arrivé
	 */
	private boolean estArrive;
	
	/**
	 * Constructeur d'une ligne droite avec une taille fixe (5)
	 * @param dep vrai si la tuile est le départ
	 * @param arri vrai si la tuile est l'arrivée
	 */
	public TuileLigneDroite(boolean dep,boolean arri) {
		super(5);
		this.estDepart = dep;
		this.estArrive=arri;
	}
	
	/**
	 * Constructeur d'une ligne droite avec une taille non fixe (pour les tets)
	 * @param nbCases nombre de cases qu'on veut sur la tuile
	 * @param dep vrai si la tuile est le départ
	 * @param arri vrai si la tuile est l'arrivée
	 */
	public TuileLigneDroite(int nbCases,boolean dep,boolean arri) {
		super(nbCases);
		this.estDepart = dep;
		this.estArrive=arri;
	}
	
	/**
	 * Méthode permettant de savoir si la tuile est le départ du circuit
	 * @return vrai si la tuile est le départ
	 */
	public boolean estDepart(){
		return (this.estDepart);
	}
	
	/**
	 * Méthode permettant de savoir si la tuile est l'arrivée du circuit
	 * @return vrai si la tuile est l'arrivée du circuit 
	 */
	public boolean estArrive(){
		return this.estArrive;
	}
}
