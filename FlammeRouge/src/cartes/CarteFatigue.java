package cartes;

import java.io.Serializable;

/**
 * classe qui permet de creer une carte fatigue a partir d'une carte
 * @author totor & blondin
 */
public class CarteFatigue  extends Carte implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * constructeur d'une carteFatigue
	 */
	public CarteFatigue(){
		super(2);
	}
	
	/* (non-Javadoc)
	 * @see Carte#toString()
	 */
	public String toString(){
		return ("Une carte fatigue "+super.toString());
	}

}