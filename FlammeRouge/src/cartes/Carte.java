package cartes;

import java.io.Serializable;

/**
 * classe qui permet de creer une carte
 * @author totor & blondin 
 */
public class Carte implements Serializable{
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * numero de la carte (nombre de cases de deplacements)
	 */
	
	private int numero;
	/**
	 * permet d'instancier une carte
	 * @param numero numero de la carte
	 */
	public Carte(int numero){
		this.numero=numero;
	}

	 /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return "de :"+this.numero;
	}

	/**
	 * getteur de numero
	 * @return le numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * setteur de numero
	 * @param numero 
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
}