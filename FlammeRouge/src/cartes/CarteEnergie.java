package cartes;

import java.io.Serializable;

/**
 * class qui cree une carteEnergie a partir d'une carte
 * @author totor & blondin
 */
public class CarteEnergie extends Carte implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * constructeur de carte energie qui donne la valeur n à la carte
	 * @param n
	 */
	public CarteEnergie(int n){
		super(n);
	}
	
	
	/* (non-Javadoc)
	 * @see cartes.Carte#toString()
	 */
	public String toString(){
		return ("Une carte energie "+super.toString());
	}

}
