package graphique;
import javax.swing.*;

import circuit.Circuit;
import java.util.List;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.*;
import principale.*;
import joueur.*;
import java.io.*;

/**
 * classe qui va creer le panel pour determiner les position des cycliste
 * @author blondin
 *
 */
public class Position extends JPanel{
	private Color tab[][];
	private static JLabel jla;
	private static boolean place;
	private static Joueur joueurc;
	private static Jeu jc;
	private static Cycliste cc;
	
	/**
	 * constructeur de Position
	 */
	public Position(){
		place = false;
		jla = new JLabel("Jouer 1 \n c'est à toi de choisir pour ton rouleur");
		this.tab = new Color[4][2];
		jc = MainGrahpique.getJeu();
		final Position c = this;
		MouseListener ml = (new MouseListener(){
			public void mouseClicked(MouseEvent e){
				c.verfiClique(e.getX(),e.getY());
			}
			public void mouseEntered(MouseEvent e){
				
			}
			public void mouseExited(MouseEvent e){
				
			}
			
			public void mousePressed(MouseEvent e){
				
			}
			
			public void mouseReleased(MouseEvent e){
				
			}
			
		});
		jla.setBorder(BorderFactory.createEmptyBorder(40, 40, 40, 40));
		jla.setFont(new Font("Helvetica", Font.PLAIN, 20));
		this.add(jla);
		this.addMouseListener(ml);
		}
	
	/**
	 * setteur des position 
	 */
	public void setPosition(){
		for(Joueur j : jc.getListeJ()){
			joueurc = j ; 
			int i =0;
			for(Cycliste c : j.getListCycliste()){
				cc=c;
				if(i==0){
					jla.setText(j.getNom()+" c'est à toi de choisir ton rouleur!");
				}
				else{
					jla.setText(j.getNom()+" maintenant choisis pour ton sprinteur!");
				}
				i++;
				while(place==false){
					this.repaint();
				}
				place=false;
			}
		}
		Fenetre.setlancerJeu();
	}
	
	
	/**
	 * methode qui verifie si le click est dans une case
	 * @param x
	 * @param y
	 */
	public void verfiClique(int x,int y){
		//si dans le rec
		if(x>getWidth()/10 && x<getWidth()-getWidth()/10 && y>getHeight()/4 && y<getHeight()-getHeight()/4){
			int larg = (getWidth()-2*getWidth()/10)/4;
			x -=getWidth()/8;
			int haut = (getHeight()-2*getHeight()/4)/2;
			y -=getHeight()/4;
			for(int i = 1;i<5;i++){
				if(larg*i>x){
					x=i;
					break;
				}
			}
			for(int i =1;i<3;i++){
				if(haut*i>y){
					y=i;
					break;
				}
			}
			if(this.tab[x-1][y-1]==null){
				this.tab[x-1][y-1]=joueurc.getCouleurGraph();
				cc.setCaseTuile(x-4);
				if(y==1){
					cc.setFileDroite(false);
				}
				place=true;
			}
		}
	}
	
	/**
	 * methode qui va afficher les cases des choix
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		int h = getHeight();
		int w = getWidth();
		g.setColor(Color.gray);
		g.fillRect(w/10, h/4, 8*w/10, h/2);
		g.setColor(Color.black);
		for(int i =2;i<=6;i++){
			if(i%2==0){
				g.drawLine(w/10, i*((h/2)/4), w-w/10, i*((h/2)/4));
			}
		}
		for(int i = 0;i <5;i++){
			g.drawLine((w/10)+i*2*w/10,h/4,(w/10)+i*2*w/10,h-h/4);
		}
		for(int i = 0;i<8;i++){
			if(i%2 == 0){
				g.setColor(Color.black);
				g.fillRect(9*w/10, h/4+i*h/16, w/10, h/8+i*h/16);
			}
			else{
				g.setColor(Color.white);
				g.fillRect(9*w/10, h/4+i*h/16, w/10, h/8+i*h/16);
			}
		}
		int larg = (getWidth()-2*getWidth()/10)/4;
		int haut = (getHeight()-2*getHeight()/4)/2;
		int startx = w/8;
		int starty= h/4;
		for(int i = 0;i<4;i++){
			for(int j = 0;j<2;j++){
				if(this.tab[i][j]!=null){
					g.setColor(this.tab[i][j]);
					g.fillOval(startx+i*larg+larg/10, starty+j*haut+haut/4, 6*larg/10, haut/2);
				}
			}
		}
		g.clearRect(0, h-h/4, w, h/4);
	}
	
	/**
	public static void main(String [] args){
		Fenetre fen = new Fenetre();
		Circuit c= null;
		try{
			c= Circuit.setCircuit("droit");
		}
		catch(IOException e){}
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(new Joueur("roger","red",Color.red));
		lj.add(new Joueur("france","vert",Color.green));
		lj.add(new Joueur("vic","bleu",Color.blue));
		lj.add(new Joueur("toto","jaune",Color.yellow));
		Jeu jeu = new Jeu(c,lj,true);
		fen.setJeu(jeu);
		fen.setPosition();
	}*/
}
