package graphique;
import javax.swing.*;
import principale.*;
import joueur.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.event.*;

/**
 * classe qui prend toute les interaction avec le plateau (en terminal)
 * @author blondin
 *
 */
public class InteractionPlateau extends JPanel {
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	private Jeu jc;
	private JLabel jActuel;
	private JPanel droite,gauche;
	private JButton sprint=new JButton("Votre sprinteur"),rouleur=new JButton("Votre rouleur");
	private static int choix = 0;
	private Plateau plat;
	private List<Joueur> lj;
	
	/**
	 * mise en place des choix d'interaction en dessous de plateau
	 * @param j
	 */
	public InteractionPlateau(Jeu j){
		this.setLayout(new BorderLayout());
		droite = new JPanel();
		gauche = new JPanel();
		this.jc=j;
		this.lj=jc.getListeJ();
		
		droite.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		droite.setPreferredSize(new Dimension(300,200));
		jActuel = new JLabel("C'est le tour de personne :)");
		
		sprint.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
		rouleur.setBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10));
		jActuel.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
		
		gauche.setBorder(BorderFactory.createLineBorder(Color.black, 2));
		gauche.setPreferredSize(new Dimension(300,200));
		List <Cycliste> classement = jc.classementCycliste();
		gauche.setLayout(new GridLayout(classement.size(),1));
		for(int i = 0; i<classement.size() ; i++){
			gauche.add(new JLabel((i+1)+": "+classement.get(i)));
		}
		droite.setLayout(new GridLayout(3,1));
		droite.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		droite.add(jActuel);
		droite.add(sprint);
		droite.add(rouleur);
		final InteractionPlateau  cc = this;
		sprint.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				choix = 1;
				cc.plat.requestFocusInWindow();
				cc.plat.setFocusable(true);
			}
		});
		rouleur.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				choix = 2;
				cc.plat.requestFocusInWindow();
				cc.plat.setFocusable(true);
			}
		});
		this.add(gauche,BorderLayout.WEST);
		this.add(droite,BorderLayout.EAST);
		//demander la pioche depuis le main
		jActuel.setText("Pas encore implanté!!");
		//jActuel.setText(jc.getListeJ().get(2).getNom());
		//System.out.println(lj.get(2).getNom());
	}
	
	/**
	 * permet de lancer une pioche de carte
	 */
	public void lancerPiocherCarte(){
		this.piocherCarte();
	}
	
	/**
	 * permet de piocher une carte
	 */
	public void piocherCarte(){
		choix = 0;
		jActuel.setText("Phase 2");
		this.revalidate();
		//String n =lj.get(2).getNom();
		//System.out.println(n);
		/**
		for(int i = 0; i <jc.getListeJ().size();i++){
			//String phrase = "C'est au tour de "+lj.get(i).getNom()+" voulez vous piocher pour:";
			//System.out.println(phrase);
			jActuel.setText("Phase 2");
			while(choix == 0){
				//wait
			}
			System.out.println("choix = "+choix);
			
		}
		*/
	}
	
	/**
	 * setteur qui permet de mettre a jour le plateau
	 * @param p
	 */
	public void setPlateau(Plateau p){
		this.plat = p;
	}
	
	/**
	 * permet d'afficher les composant graphique
	 */
	public void paintComponents(){
		
	}

}
