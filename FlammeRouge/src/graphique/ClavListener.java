
package graphique;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


/**
 * cette classe va permettre d'ecouter le clavier
 * @author blondin
 *
 */
public class ClavListener implements KeyListener{
	Plateau dess=null;
	
	/**
	 * methode qui va demander a deplacer les objets graphique en fonction de la touche utilisé
	 */
	public void keyPressed(KeyEvent e) {
		
		
		this.dess = (Plateau) e.getSource();
		System.out.println(e.getKeyCode());
		//droite 39
		if(e.getKeyCode()==39)
			this.dess.ZPlus(1);
		
		//bas 40
		if(e.getKeyCode()==40)
			dess.ZPlus(2);
		
		//gauche 37
		if(e.getKeyCode()==37)
			dess.ZPlus(3);
		
		//haut 38
		if(e.getKeyCode()==38){
			dess.ZPlus(4);
		}
		this.dess.requestFocusInWindow();
		this.dess.repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
