package graphique;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import joueur.*;

import circuit.*;


import principale.Jeu;

/**
 * class qui va creer l'affichage du jeu
 * @author blondin
 *
 */
public class Plateau extends JPanel{

	//hauteur de la fenetre
	private int h;
	//largeur de la fenetre
	private int l;
	
	//rotation de l'image (a modifier en fonctione du precedent et du type)
	private int rotation=0;
	
	//liste des tuiles a analyser
	private static List<Tuile> tabTuile;
	//liste des joueur a afficher
	//liste de TuileGraphique a afficher
	private List<TuileGraphique> listGraphique;
	private List<Point> listTerrain;
	private static List<Cycliste> listCycliste;
	private static List<Joueur> listJ;
	private List<JoueurGraph> lJGraph;
	
	private boolean direction;
	private JButton plus,moin;
	private BufferedImage img=null;

	
	/**
	 * constructeur de plateau
	 * @param h hauteur
	 * @param l largeur
	 */
	public Plateau(int h, int l) {
		super();
		this.h=h;
		this.l=h;
		tabTuile=new ArrayList<Tuile>();
		listGraphique=new ArrayList<TuileGraphique>();
		listTerrain=new ArrayList<Point>();
		lJGraph=new ArrayList<JoueurGraph>();
		this.rotation=0;

		this.requestFocusInWindow ();
		this.setFocusable(true);
		
		ClavListener kl=new ClavListener();
		this.addKeyListener(kl);
		
		for(int i=0;i<4;i++){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			init();
		}
		
		JOptionPane jop1 = new JOptionPane();
		jop1.showMessageDialog(null, "Si vous voulez vous deplacer sur la carte, appuyer sur tab puis les fleches", "Information", JOptionPane.INFORMATION_MESSAGE);
		

	}
	

	/**
	 * methode qui initialise le terrain avec des image d'herbe de 100 par 100
	 */
	public void init(){
		for(int j=-50;j<=50;j++){
			for(int i=-50;i<=50;i++){
				listTerrain.add(new Point(j*100,i*100));
				
			}
		}
	}
	
	/**
	 * methode qui permet selon l'entrée de ce deplacer sur la carte (en deplaceant les objet graphiques)
	 * @param i
	 */
	public void ZPlus(int i){
		int x1,y1;
		for(TuileGraphique t: this.listGraphique){
			
			x1=(int)t.getCoordonne().getX();
			y1=(int)t.getCoordonne().getY();
			if(i==1){x1-=50;}
			if(i==2){y1-=50;}
			if(i==3){x1+=50;}
			if(i==4){y1+=50;}
			t.setCoordonne(new Point(x1,y1));
			
		}

		for(Point p:this.listTerrain){
			if(i==1){p.setLocation(p.getX()-50, p.getY());}
			if(i==2){p.setLocation(p.getX(),p.getY()-50);}
			if(i==3){p.setLocation(p.getX()+50,p.getY());}
			if(i==4){p.setLocation(p.getX(),p.getY()+50);}
			
		}
		
		for(JoueurGraph c: this.lJGraph){
			x1=(int)c.getx();
			y1=(int)c.gety();
			if(i==1){x1-=50;}
			if(i==2){y1-=50;}
			if(i==3){x1+=50;}
			if(i==4){y1+=50;}
			c.setx(x1);
			c.sety(y1);
		}
	
	}

	/**
	 * methode qui affiche dans le JPanel
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		this.h=this.getHeight();
		this.l=this.getWidth(); 
		//ici on reinitialise les variable de placement de case, afin de pouvoir raffraichir
		this.rotation=0;
		
				
		//si on a choisit le circuit, on peu le dessiner
		if(!this.tabTuile.isEmpty()){
			//les dessins seront noir
			g.setColor(Color.black);
			//on créer le tableau
			this.principale(g);
			//on affiche les tuiles de la liste
			this.affichage(g);
			if(!this.lJGraph.isEmpty()){
				translate();
				affichageJoueur(g);
			}
		}
	}
	/**
	 * methode qui va traduire les cycliste des joueurs en CyclisteGraphique //non fini
	 */
	public void translate(){
		for(Joueur j: this.listJ){
			for(Cycliste c : this.listCycliste){
				if(c instanceof Rouleur){
					this.lJGraph.add(new JoueurGraph(0,0,j.getCouleurGraph(),true));
				}else{
					this.lJGraph.add(new JoueurGraph(0,0,j.getCouleurGraph(),false));
				}
				if(c.getPositionActuelle()>=0){
					this.listTerrain.get(c.getPositionActuelle());
				}
			}
		}
	}
	
	/**
	 * methode qui affichera les joueurs
	 * @param g
	 */
	public void affichageJoueur(Graphics g){
	}
	
	public void affichage(Graphics g){
		TuileGraphique t=null;
		for(int i=0;i<this.listGraphique.size();i++){
			t=this.listGraphique.get(i);
			//on affiche des ligne delimitant un plateau de jeu
			g.drawImage(t.getImage(),(int) t.getCoordonne().getX(),(int) t.getCoordonne().getY(),null);
		}
	}
	
	
	
	/**
	 * methode qui va dessiner le decor du Plateau
	 * @param g
	 */
	public void principale(Graphics g){
		String path=new File("").getAbsolutePath();
		BufferedImage image=null;
		try {
			image = ImageIO.read(new File(path+"/src/image/Terrain.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		for(Point p:this.listTerrain){
			g.drawImage(image,(int)p.getX(),(int)p.getY(),null);
			
		}
		
	}
	
	
	/**
	 * dans cette methode, nous allons traduire toute la lite des Tuiles en TuileGraphique afin de pouvoir les placer plus facilement
	 * 
	 */
	public void traduction(){
		//on initialise le premier point
		Point p=new Point(0,0);
		//on initialise virage a faux
		boolean virge=false;
		//on init image a null
		BufferedImage img=null;
		//tuile precedente
		int precedente=0;
		
				
		//pour toute les tuiles de la tab de tuile
		for(Tuile t : tabTuile){
			//si t est un virage
			if(t instanceof TuileVirage){
				//on recupere la direction (true = droite)
				this.direction=((TuileVirage) t).getDirection();
				//virage est vrai
				virge=true;
				//on calcule le prochain point
				p=prochainPosVirage((int)p.getX(),(int)p.getY(),((TuileVirage) t).estLege(),precedente);
				if(((TuileVirage) t).estLege()){
					precedente=0;
				}else{
					precedente=1;
				}
			}else if(t instanceof TuileLigneDroite){
				virge=false;
				p=prochainPosDroit((int)p.getX(),(int)p.getY(),precedente);
				precedente=-1;	
			}
							
			this.listGraphique.add(new TuileGraphique(p,virge,this.rotation,this.img,t.getNbCases()));
		}
	}
	
	

	


	/**
	 * methode qui permet de connaitre le point initiale de la prochaine TuileGraphque si c'est une droite
	 * @param x ordronnée
	 * @param y abscisse
	 * @param prec precedent
	 * @return le point
	 */
	public Point prochainPosDroit(int x,int y,int prec){
		Point p=null;
		//si ce n'est pas un virage 

		//on regarde la rotation et on dit en fonction quelle image de route droite utiliser
		switch(Math.abs(this.rotation)){
		case 0:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x+58;
				}else{
					x=x+58;
					y=y+7;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				x=x+50;
			}else if(prec==-1){
				//sinon
				x=x+60;
			}
			this.img=imageTuile();
			break;
		case 1:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				x=x+20;
				y=y+22;
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				x=x+15;
				y=y+35;
			}else if(prec==-1){
				//sinon
				x=x+30;
				y=y+30;
			}
			this.img=imageTuile();
			break;
		case 2:
			
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x+7;
					y=y+110;
				}else{
					y=y+58;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(this.direction){
					y=y+50;
				}else{
					y=y+50;
				}
			}else if(prec==-1){
				if(this.direction){
					y=y-50;
				}else{
					y=y+60;
				}
			}		
			
			
			
			this.img=imageTuile();
			break;
		case 3:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x-42;
					y=y+20;
				}else{
					x=x-42;
					y=y+20;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(!this.direction){
					x=x-40;
					y=y+20;
				}else{
					x=x-44;
					y=y+34;
				}
			}else if(prec==-1){
				if(!this.direction){
					x=x-40;
					y=y+40;
				}else{
					x=x-44;
					y=y+40;
				}
			}				
			this.img=imageTuile();
			break;
		case 4:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x-60;
					y=y+7;
				}else{
					x=x-60;
					y=y-0;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(this.direction){
					x=x-60;
				}else{
					x=x-60;
				}
			}else if(prec==-1){
				x=x-60;
			}		
			this.img=imageTuile();
			break;
		case 5:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x-40;
					y=y-42;
				}else{
					x=x-42;
					y=y-40;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(this.direction){
					x=x-42;
					y=y-40;
				}else{
					x=x-44;
					y=y-34;
				}
			}else if(prec==-1){
				if(this.direction){
					x=x-35;
					y=y-35;
				}else{
					x=x-35;
					y=y-35;
				}
			}				
			this.img=imageTuile();
			break;
		case 6:
			
			 //si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					y=y-60;
					x=x+0;
				}else{
					y=y-58;
					x=x+7;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(!this.direction){
					
					y=y-60;
				}else{
					x=x+44;
					y=y-34;
				}
			}else if(prec==-1){
				y=y-58;
			}
			
			
			this.img=imageTuile();
			break;
		case 7:
			//si jamais la tuile precedente est une tuile legere
			if(prec==0){
				if(this.direction){
					x=x+22;
					y=y-40;
				}else{
					x=x+22;
					y=y-40;
				}
				//si jamais la tuile precedente est une tuile serre
			}else if(prec==1){
				if(!this.direction){
					x=x+40;
					y=y-20;
				}else{
					x=x+15;
					y=y-40;
				}
			}else if(prec==-1){
				if(!this.direction){
					x=x+43;
					y=y-43;
				}else{
					x=x+40;
					y=y-40;
				}
			}
			this.img=imageTuile();
			break;
		default:
			break;	
		}
		//on créer un point avec
		return new Point(x,y);
	}
	
	/**
	 * methode qui permet de connaitre le point initiale de la prochaine TuileGraphque si c'est un virage
	 * @param x
	 * @param y
	 * @param rot
	 * @param estLege
	 * @return
	 */
	private Point prochainPosVirage(int x, int y,boolean lege,int prec) {
		Point p=null;
		if(lege){
			//on regarde la rotation et on dit en fonction quelle image de route droite utiliser
			//mais en plus on modifie la rotation en fonction du virage leger ou non
			switch(Math.abs(this.rotation)){
			case 0:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					if(this.direction){
						x=x+58;
					}else{
						x=x+58;
						y=y+7;
					}
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						x=x+50;
					}else{
						x=x+50;
					}
				}else if(prec==-1){
					if(this.direction){
						x=x+60;
						
					}else{
						x=x+60;
						y=y-7;					
					}
				}
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 1:
				if(this.direction){
					x=x+22;
					y=y+20;
				}else{
					x=x+22;
					y=y+22;
				}
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 2:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					if(this.direction){
						y=y+60;
					}else{
						y=y+60;
					}
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						y=y+60;
					}else{
						y=y+60;
					}
				}else if(prec==-1){
					if(this.direction){
						y=y+110;
						x=x-7;
					}else{
						y=y+60;
					}
				}
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 3:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					if(this.direction){
						x=x-22;
						y=y+22;
					}else{
						x=x-22;
						y=y+22;
					}
					
					//
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						x=x-22;
						y=y+42;
					}else{
						x=x-22;
						y=y+22;
					}
				}else if(prec==-1){
					if(this.direction){
						x=x-22;
						y=y+42;
					}else{
						x=x-22;
						y=y+42;
					}
				}
				
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 4:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					if(this.direction){
						x=x-58;
					}else{
						x=x-57;
						
					}
					
					//
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						x=x-60;
						y=y-7;
					}else{
						x=x-57;
					}
				}else if(prec==-1){
					if(this.direction){
						x=x-60;
						y=y-7;
					}else{
						x=x-57;
					}
				}
				
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 5:
				if(prec==0){
					if(this.direction){
						x=x-22;
						y=y-22;
					}else{
						x=x-22;
						y=y-22;
					}				
					//
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						x=x-45;
						y=y-45;
					}else{
						x=x-22;
						y=y-22;
					}
					
				}else if(prec==-1){
					if(this.direction){
						x=x-22;
						y=y-22;
					}else{
						x=x-22;
						y=y-22;
					}
					
				}
				
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 6:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					y=y-50;
					
					//
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						y=y-57;
					}else{
						y=y-60;
						x=x-7;
					}
				}else if(prec==-1){
					if(this.direction){
						y=y-57;
						
					}else{
						y=y-60;
						x=x-7;
					}
				}
				
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			case 7:
				 //si jamais la tuile precedente est une tuile legere
				if(prec==0){
					x=x+22;
					y=y-20;
					
					//
				//si jamais la tuile precedente est une tuile serre
				}else if(prec==1){
					if(this.direction){
						x=x+22;
						y=y-20;
					}else{
						x=x+22;
						y=y-20;
					}
				}else if(prec==-1){
					if(this.direction){
						x=x+42;
						y=y-20;
					}else{
						x=x+42;
						y=y-22;
					}
				}
				
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+1;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-1;
				}
				break;
			default:
				break;	
			}
		}else{
			switch(Math.abs(this.rotation)){
			case 0:
				x=x+50;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 1:
				x=x+43;
				y=y+43;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 2:
				y=y+50;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 3:
				if(this.direction){
					x=x-40;
					y=y+42;
				}
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 4:
				x=x-50;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 5:
				if(this.direction){
					y=y-37;
					x=x-17;
				}else{
					x=x-20;
					y=y-20;
				}
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 6:
				y=y-50;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			case 7:
				x=x+40;
				y=y+40;
				if(this.direction){
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation+2;
				}else{
					this.img=imageTuileVirage(lege);
					this.rotation=this.rotation-2;
				}
				break;
			default:
				break;	
			}	
		}
		
		if(this.rotation==8){
			this.rotation=0;
		}else if(this.rotation==9){
			this.rotation=1;
		}
		if(this.rotation==-1){
			this.rotation=7;
		}else if(this.rotation==-2){
			this.rotation=6;
		}
		//on créer un nouveau point avec les coordonnées
		p=new Point(x,y);
		return p;
		
	}

	/**
	 * cette methode va verifier quelle image donner a la tuile
	 * @return image
	 */
	public BufferedImage imageTuile(){
		BufferedImage image=null;
		//on recupere le chemin absolu
		String path=new File("").getAbsolutePath();
		//on donne l'image de fond du menu
		try{
			switch(Math.abs(this.rotation)){
			case 0:
				image = ImageIO.read(new File(path+"/src/image/L0.png"));
				break;
			case 1:
				image = ImageIO.read(new File(path+"/src/image/l7.png"));
				break;
			case 2:
				image = ImageIO.read(new File(path+"/src/image/L2.png"));
				break;
			case 3:
				image = ImageIO.read(new File(path+"/src/image/l1.png"));
				break;
			case 4:
				image = ImageIO.read(new File(path+"/src/image/L4.png"));
				break;
			case 5:
				image = ImageIO.read(new File(path+"/src/image/l3.png"));
				break;
			case 6:
				image = ImageIO.read(new File(path+"/src/image/L6.png"));
				break;
			case 7:
				image = ImageIO.read(new File(path+"/src/image/l5.png"));
				break;
			default:
				break;	
			}
		}
		catch(IOException e){
			System.out.println("probleme d'ouverture de l'image");
		}
		return image;
	}
	
	/**
	 * cette methode va verifier quelle image donner a la tuile virage en fonction de si c'est un virage leger ou non
	 * @param estLege
	 * @return
	 */
	private BufferedImage imageTuileVirage(boolean lege) {
		BufferedImage image=null;
		//on recupere le chemin absolu
		String path=new File("").getAbsolutePath();
		//on donne l'image de fond du menu
		System.out.println(Math.abs(this.rotation));
		try{
			if(!lege){
				switch(Math.abs(this.rotation)){
				case 0:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/0.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/2.png"));
					}
					break;
				case 1:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/7.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/1.png"));
					}
					break;
				case 2:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/2.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/4.png"));
					}
					break;
				case 3:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/1.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/3.png"));
					}
					break;
				case 4:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/4.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/6.png"));
					}
					break;
				case 5:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/3.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/5.png"));
					}
					break;
				case 6:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/6.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/0.png"));
					}
					break;
				case 7:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/5.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/7.png"));
					}
					break;
				default:
					break;	
				}
				
			}else{
				switch(Math.abs(this.rotation)){
				case 0:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLD2.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLG2.png"));
					}
					break;
				case 1:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLG1.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLD0.png"));
					}
					break;
				case 2:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLD3.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLG0.png"));
					}
					break;
				case 3:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLG2.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLD1.png"));
					}
					break;
				case 4:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLD0.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLG3.png"));
					}
					break;
				case 5:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLG0.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLD2.png"));
					}
					break;
				case 6:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLD1.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLG1.png"));
					}
					break;
				case 7:
					if(this.direction){
						image = ImageIO.read(new File(path+"/src/image/VLG3.png"));
					}else{
						image = ImageIO.read(new File(path+"/src/image/VLD3.png"));
					}
					break;
				default:
					break;	
				}
			}
			
		}
		catch(IOException e){
			System.out.println("probleme d'ouverture de l'image");
		}
		return image;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
		/**
	 * cette methode va initialiser la liste de Tuile
	 * @param tt Liste de Tuile
	 */
	public static void setTuile(List<Tuile> tt){
		tabTuile=tt;
		//on traduit les Tuiles en tuileGrapique
	}
	
	/**
	 * cette methode va initialiser la liste de joueur
	 * @param lj Liste de Joueur
	 */
	public static void setJoueur(List<Joueur> lj) {
		listJ=lj;
	}	
	
	/**
	 * methode qui permet de rendre la nouvelle version du plateau
	 */
	public void rendre(){
		this.repaint();
	}
}
