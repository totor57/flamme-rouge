package graphique;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;

import principale.MainGrahpique;

/**
 * classe qui va creer un Panel pour le menu
 * @author blondin
 *
 */
public class Menu extends JPanel {
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;
	private JButton regles,jouer,sauvegarde,score;
	private BufferedImage image;
	private static String pos;
	//
	private static Menu mcCourant;
	//Les éléments du jeu
	private static String choixCircuit;
	private static String listeNom[];
	private static boolean montage;
	private static int nbj= 0;
	
	/**
	 * constructeur de Menu
	 */
	public Menu(){
		//fen = (Fenetre) SwingUtilities.getWindowAncestor(this);
		//System.out.println(fen);
		pos="Accueil";
		this.dessinerAccueil();
	}
	
	/**
	 * permet d'effacer un menu
	 * @param m
	 */
	public static void effacer(Menu m){
		m.removeAll();
		m.revalidate();
		m.repaint();
	}
	
	/**
	 * permet de dessiner l'acceuil
	 */
	public void dessinerAccueil(){
		//on recupere le chemin absolu
		String path=new File("").getAbsolutePath();
		//on donne l'image de fond du menu
		try{
			image = ImageIO.read(new File(path+"/src/image/fondMenu.png"));
		}
		catch(IOException e){
			System.out.println("probleme d'ouverture de l'image");
		}
		
		//on fait une grille de boutons
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		//separé par 10 en y
		gbc.insets = new Insets(10, 0, 10, 0);
		gbc.gridx=0;
		gbc.gridy=0;
		gbc.gridheight=1;
		gbc.gridwidth=1;
		
		//on init un bouton jouer
		jouer=new JButton("jouer");
		//le menu courant sera celui ci
	    Menu.mcCourant = this;
	    //si jouer est activer on efface et dessine jouer
		jouer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Menu.pos="Jouer";
				Menu.effacer(mcCourant);
				mcCourant.dessinerJouer();
			}
		});
		//si regle est activé on efface et dessine regle
		regles=new JButton("regles");
		regles.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Menu.pos="Regles";
				Menu.effacer(mcCourant);
				mcCourant.dessinerRegles();
			}
		});
		//save
		sauvegarde=new JButton("sauvegarde");
		//score
		score=new JButton("scores");
		//TODO.........
		
		
		//dimensions
		jouer.setPreferredSize(new Dimension(200,50));
		sauvegarde.setPreferredSize(new Dimension(200,50));
		regles.setPreferredSize(new Dimension(200,50));
		score.setPreferredSize(new Dimension(200,50));
		gbc.gridy=1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		this.add(jouer,gbc);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridy=3;
		//gbc.gridy=1;
		this.add(regles,gbc);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridy=5;
		this.add(sauvegarde,gbc);
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.gridy=7;
		this.add(score,gbc);
	}
	
	/**
	 * methode qui dessine le parametrage de la partie
	 */
	public void dessinerJouer(){
		Menu.montage=false;
		this.setLayout(new BorderLayout());
		final JPanel haut = new JPanel();
		final JPanel bas = new JPanel();
		bas.setBorder(BorderFactory.createEmptyBorder(20, 20, 30, 30));
		JPanel basbas = new JPanel(); 
		final JTextField j1 = new JTextField("");
		j1.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.BLUE));
		final JTextField j2 = new JTextField("");
		j2.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.red));
		final JTextField j3 = new JTextField("");
		j3.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.green));
		final JTextField j4 = new JTextField("");
		j4.setBorder(BorderFactory.createMatteBorder(4,4,4,4,Color.yellow));
		final JLabel j1text = new JLabel("NomJoueur 1");
		final JLabel j2text = new JLabel("Nom Joueur 2");
		final JLabel j3text = new JLabel("Nom jouer 3");
		final JLabel j4text = new JLabel("Nom jouer 4");
		bas.setLayout(new GridLayout(4,2,100,100));
		j1.setSize(100, 20);
		/**
		 * choix du circuit
		 */
		final JComboBox<String> c = new JComboBox<String>(new String[] {"choix du circuit","test","testCourt","droit","Wallonie","Sprint","Vittel"});
		c.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				switch(c.getSelectedIndex()){
				case 0:
					Menu.choixCircuit=null;
					break;
				case 1:
					Menu.choixCircuit="circuitTest";
					break;
				case 2:
					Menu.choixCircuit="circuitTestCourt";
					break;
				case 3:
					Menu.choixCircuit="droit";
					break;
				case 4:
					Menu.choixCircuit="Plateau_de_Wallonie";
					break;
				case 5:
					Menu.choixCircuit="Sprint_de_Montagne";
					break;
				case 6:
					Menu.choixCircuit="Vittel_la_planche_des_belles_filles";
					break;
				default:
					
				}
			}
		});
		
		/**
		 * choix du nombre de joueur
		 */
		final JComboBox j = new JComboBox(new String[] {"nombres joueurs","2","3","4"});
		j.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				bas.removeAll();
				bas.revalidate();
				bas.repaint();
				switch(j.getSelectedIndex()){
				case 0:
					Menu.nbj=0;
					break;
				case 1:
					Menu.nbj=2;
					bas.setLayout(new GridLayout(2,2,100,100));
					bas.add(j1text);
					bas.add(j1);
					bas.add(j2text);
					bas.add(j2);
					break;
				case 2:
					Menu.nbj=3;
					bas.setLayout(new GridLayout(3,2,100,100));
					bas.add(j1text);
					bas.add(j1);
					bas.add(j2text);
					bas.add(j2);
					bas.add(j3text);
					bas.add(j3);
					break;
				case 3:
					Menu.nbj=4;
					bas.setLayout(new GridLayout(4,2,100,100));
					bas.add(j1text);
					bas.add(j1);
					bas.add(j2text);
					bas.add(j2);
					bas.add(j3text);
					bas.add(j3);
					bas.add(j4text);
					bas.add(j4);
					break;
				default:
					break;
					
				}
			}
		});
		
		/**
		 * bouton d'interaction dans les menu
		 */
		JButton jouer = new JButton("Jouer");
		JButton retour = new JButton("retour");
		jouer.setForeground(Color.white);
		jouer.setBackground(Color.gray);
		retour.setForeground(Color.white);
		retour.setBackground(Color.gray);
		
		/**
		 * si il manque qqchose
		 */
		jouer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				JOptionPane jop;
				if(Menu.nbj <2){
					jop = new JOptionPane();
					JOptionPane.showMessageDialog(null, "Vous devez séléctionner votre nombre de joueurs!", "Erreur", JOptionPane.ERROR_MESSAGE);
				}
				else{
					if(Menu.choixCircuit==null){
					jop = new JOptionPane();
					JOptionPane.showMessageDialog(null, "Il faut séléctionner votre circuit!", "Erreur", JOptionPane.ERROR_MESSAGE);
					}
					else{
						boolean nom = true;
						int i =0;
						Menu.listeNom = new String[Menu.nbj];
						for(Component c : bas.getComponents()){
							if(c instanceof JTextField){
								if(((JTextField)c).getText().equals("")){
									nom=false;
								}
								else{
									Menu.listeNom[i]=((JTextField) c).getText();
									i++;
								}
							}
						}
						if(nom){
							//on créer un jeu avec les choix
							MainGrahpique.genererJeu(Menu.choixCircuit, Menu.montage, Menu.listeNom);
						}
						else{
							jop = new JOptionPane();
							jop.showMessageDialog(null, "Il faut entrer des noms!", "Erreur", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		});
		
		/**
		 * si retour on revient au precedent
		 */
		retour.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Menu.pos="Accueil";
				Menu.effacer(mcCourant);
				mcCourant.dessinerAccueil();
			}
		});
		
		// la montagne?
		final JCheckBox montage = new JCheckBox("Générer une étape de montage");
		montage.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				AbstractButton abstractButton = (AbstractButton) e.getSource();
			    if(abstractButton.getModel().isSelected()){
			    	Menu.montage=true;
			    }
			    else{
			    	Menu.montage=false;
			    }
			}
		});
		haut.add(c);
		haut.add(j);
		haut.add(montage);
		haut.setBackground(Color.gray);
		basbas.add(retour);
		basbas.add(jouer);
		this.add(haut,BorderLayout.NORTH);
		this.add(bas,BorderLayout.CENTER);
		this.add(basbas,BorderLayout.SOUTH);
	}
	
	/**
	 * on dessine les regle
	 */
	public void dessinerRegles(){
		JPanel contenant = new JPanel();
		JPanel bas = new JPanel();
		final HautRegles haut = new HautRegles();
		this.setLayout(new BorderLayout());
		haut.setBorder(BorderFactory.createMatteBorder(20,30,0,30,new Color(192,90,69)));
		bas.setBackground(new Color(192,90,69));
		
		String path=new File("").getAbsolutePath();
		try{
			this.image = ImageIO.read(new File(path+"/src/image/regles.png"));
		}
		catch(IOException e){
			System.out.println("Il y a eu un soucis dans la lecture de l'image!");
		}
		
		JButton retour = new JButton("retour");
		retour.setForeground(Color.white);
		retour.setBackground(Color.gray);
		retour.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Menu.pos="Accueil";
				Menu.effacer(mcCourant);
				mcCourant.dessinerAccueil();
			}
		});
		bas.add(retour);
		final Menu m = this;
		JScrollBar sp = new JScrollBar(JScrollBar.VERTICAL, 30, 40, 0, 300);
		sp.addAdjustmentListener(new AdjustmentListener(){
			public void adjustmentValueChanged(AdjustmentEvent evt) {
				m.repaint();
			}
		});
		contenant.setLayout(new BorderLayout());
		haut.setLayout(new BorderLayout());
		haut.add(sp,BorderLayout.EAST);
		contenant.add(haut,BorderLayout.CENTER);
		this.add(contenant,BorderLayout.CENTER);
		this.add(bas,BorderLayout.SOUTH);
		
	}
	
	/**
	 * on dessine le jeu
	 */
	public void paintComponent(Graphics g){
		super.paintComponents(g);
		//on clear pour redessiner
		g.clearRect(0, 0, getWidth(), getHeight());
		//si acceuil
		if(Menu.pos.equals("Accueil")){
			//police des lettres
			Font police = new Font("Arial", Font.BOLD, 30);
			g.setFont(police);
			//on draw l'image definit precedemment
			g.drawImage(image, 0, 0,null);
			
			g.setColor(Color.white);
			g.drawString("FLAMME ROUGE", getWidth()/4-80, getHeight()/6-10);
			g.drawString("Le jeu!", getWidth()/4-80, getHeight()/6+30);
			police = new Font("TimesRoman", Font.BOLD, 30);
		}
	}
	
	/**
	 * classe qui permet de dessiner l'image des regle
	 * @author blondin
	 *
	 */
	public class HautRegles extends JPanel{
		public void paintComponent(Graphics g){
			g.drawImage(image, 0, 0,null);
		}
	}
	
}
