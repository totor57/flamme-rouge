package graphique;

import javax.swing.*;
import java.awt.*;
import principale.Jeu;

/**
 * classe qui creer la fenetre dans laquelle on affichera des JPanel
 * @author blondin
 *
 */
public class Fenetre extends JFrame{
		
	private static final long serialVersionUID = 1L;
	
	/**
	 * Jeu duquel on tire les elements a afficher
	 */
	private Jeu jeuC;
	private int l=600;
	private int h=600;
	private JPanel dess = new JPanel();
	private static boolean lancerJeu=false;
	private InteractionPlateau interac;
	
	/**
	 * on créer une fenetre
	 */
	public Fenetre(){		
		
		//taille de la fenetre		
		this.setTitle("Menu");
		this.setSize(l,h);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		this.setResizable(false);
		//on créer un affichage
		this.dess =new Menu();
		//on donne la dimension a la fenetre(et son contenu)
		((Component) dess).setPreferredSize(new Dimension(l,h));
		//on donne le contenu de la fenetre
		this.setContentPane((Container) dess);
		this.pack();
	    this.setVisible(true);
	}
	
	
	/**
	 * on va rendre le jeu (plateau) on va donc donner les liste de tuile et de joueur afin de les afficher dans Plateau
	 * @param jeuC jeu de la fenetre
	 */
	public void rendre(Jeu jeuC){
		this.setLayout(new GridLayout(1,1));
		this.jeuC=jeuC;
		Plateau.setJoueur(jeuC.getListeJ());
		Plateau.setTuile(jeuC.getPiste().getListTuile());
		((Plateau)dess).rendre();
		((Plateau) this.dess).traduction();
		this.dess.repaint();
	}
	
	/**
	 * on donne au Panel de fenetre le panel courant (Menu,Position ou Plateau)
	 * @param j
	 */
	public void setCourant(JPanel j){
		this.dess=j;
		((Component) dess).setPreferredSize(new Dimension(l,h));
		this.setContentPane((Container) dess);
		this.setVisible(true);
	}
	
	/**
	 * on creer un plateau
	 */
	public void setPlateau(){
		this.getContentPane().removeAll();
		this.dess=null;
		this.setSize(l,h+200);
		this.setTitle("Plateau");
		this.dess=new Plateau(l,h);
		((Component) dess).setPreferredSize(new Dimension(l,h));
		InteractionPlateau carte= new InteractionPlateau(jeuC);
		carte.setPlateau((Plateau)dess);
		this.interac=carte;
		((Component) carte).setPreferredSize(new Dimension(l,300));
		this.setLayout(new BorderLayout());
		this.add(dess,BorderLayout.CENTER);
		this.add(carte,BorderLayout.SOUTH);
		this.setVisible(true);
		this.dess.repaint();
	}
	
	/**
	 * on Créer un menu 
	 */
	public void setMenu(){
		if(!(dess instanceof Menu)){
			dess = new Menu();
		}
		((Component) dess).setPreferredSize(new Dimension(l,h));
		this.setContentPane((Container) dess);
		this.setVisible(true);
	}
	
	/**
	 * nous allons créer un panel pour choisir la postion des cyclistes
	 */
	public void setPosition(){
		lancerJeu=false;
		this.setTitle("Position");
		dess=new Position();
		((Component) dess).setPreferredSize(new Dimension(l,h));
		this.setContentPane((Container) dess);
		this.setVisible(true);
		((Position)dess).setPosition();
	}
	
	/**
	 * setteur de Jeu
	 * @param j jeu
	 */
	public void setJeu(Jeu j){
		this.jeuC=j;
	}
	
	/**
	 * methode qui permet de piocher une carte
	 */
	public void piocherCarte(){
		interac.piocherCarte();
	}
	
	/**
	 * getteur du jeu dans fenetre
	 * @return la fenetre
	 */
	public static boolean getlancerJeu(){
		return Fenetre.lancerJeu;
	}
	
	/**
	 * setteur du jeu dans fenetre
	 */
	public static void setlancerJeu(){
		Fenetre.lancerJeu=true;
	}
}

