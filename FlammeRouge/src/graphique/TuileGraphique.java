package graphique;

import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 * classe qui creer des tuiles graphique, permettant d'afficher les tuiles après conversion
 * @author blondin
 *
 */
public class TuileGraphique {
	
	//coordonée initiale de la tuileGraphique
	private Point coordonne;
	//est ce un virage?
	private boolean virage;
	//la rotation qui lui a ete donné (permet de definir le sens)
	private int rotation;
	//image de la TuileGraphique
	private BufferedImage image;
	//nombre de place de la TuileGraphique
	private int nbPlace;
	
	/**
	 * constructeur de TuileGraphique
	 */
	public TuileGraphique(Point c,boolean v,int r,BufferedImage i,int nb){
		this.coordonne=c;
		this.virage=v;
		this.rotation=r;
		this.image=i;
		this.nbPlace=nb;
	}

	/**
	 * getteur qui retourne les coordonée
	 * @return les coordonnes
	 */
	public Point getCoordonne() {
		return coordonne;
	}

	/**
	 * methode qui permet de mettre a jour les coordonée (le Point initiale de la tuile Graphique)
	 * @param coordonne
	 */
	public void setCoordonne(Point coordonne) {
		this.coordonne = coordonne;
	}

	/**
	 * methode qui retourne true ou false en fonction de si la tuileGraphique est un virage
	 * @return vrai si est un virage
	 */
	public boolean isVirage() {
		return virage;
	}

	/**
	 * methode qui permet de mettre a jour une tuile pour qu'elle soit ou non un virage
	 * @param virage
	 */
	public void setVirage(boolean virage) {
		this.virage = virage;
	}

	/**
	 * getteur de la rotation
	 * @return rotation courante
	 */
	public int getRotation() {
		return rotation;
	}

	/**
	 * setteur de la rotation
	 * @param rotation
	 */
	public void setRotation(int rotation) {
		this.rotation = rotation;
	}

	/**
	 * getteur de l'image de la tuile
	 * @return l'image
	 */
	public BufferedImage getImage() {
		return image;
	}
	
	/**
	 * setteur de l'image
	 * @param image
	 */
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}

	/**
	 * getteur du nombre de place de la tuile
	 * @return le nombre de place
	 */
	public int getNbPlace() {
		return nbPlace;
	}

	/**
	 * setteur du nombre de place de la tuile
	 * @param nbPlace
	 */
	public void setNbPlace(int nbPlace) {
		this.nbPlace = nbPlace;
	}
}
