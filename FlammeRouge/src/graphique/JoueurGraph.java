package graphique;

import java.awt.Color;

/**
 * classe qui va permettre de stocker des information des cycliste afin de les afficher
 * @author blondin
 *
 */
public class JoueurGraph {
	private int x;
	private int y;
	private Color c;
	private boolean rouleur;
	
	/**
	 * constructeur
	 * @param x position en x
	 * @param y position en y
	 * @param c couleur
	 * @param cour rouleur ou sprinteur
	 */
	public JoueurGraph(int x,int y,Color c,boolean cour){
		this.x=x;
		this.y=y;
		this.c=c;
		this.rouleur=cour;
	}
	
	/**
	 * getteur de x
	 * @return l'ordonnee
	 */
	public int getx(){
		return this.x;
	}
	
	/**
	 * getteur de y
	 * @return l'abscisse
	 */
	public int gety(){
		return this.y;
	}
	
	/**
	 * getteur de la couleur
	 * @return la couleur
	 */
	public Color getc(){
		return this.c;
	}

	/**
	 * setteur de x
	 * @param x1
	 */
	public void setx(int x1) {
		this.x=x1;
	}
	
	/**
	 * setteur de y
	 * @param y1
	 */
	public void sety(int y1) {
		this.y=y1;
	}
	
	/**
	 * getteur de l'etat du cycliste, rouleur ou sprinteur
	 * @return le rouleur
	 */
	public boolean getRouleur(){
		return this.rouleur;
	}
}
