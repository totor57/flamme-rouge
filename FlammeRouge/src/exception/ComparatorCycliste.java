package exception;
import java.util.*;
import joueur.*;

/**
 * Classe de comparation d'un cycliste
 * @author totor & blondin
 *
 */
public class ComparatorCycliste implements Comparator<Cycliste>{
	
	/**
	 * methode de comparaison entre deux cyclistes pour savoir lequel est le plus loin sur le circuit
	 */
	public int compare(Cycliste a, Cycliste b){
		int res = 0;
		if(a.getCasePasse() > b.getCasePasse()){
			res = -1;
		}
		else if(a.getCasePasse() < b.getCasePasse()){
			res = 1;
		}
		return res;
		
	}
}
