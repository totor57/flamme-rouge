package exception;

/**
 * Classe d'exception pour la fin du jeu
 * @author totor & blondin
 *
 */
public class FinDeJeuException extends Exception {
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur pour creer une exception avec un message d'erreur personnalise 
	 * @param n message d'erreur
	 */
	public FinDeJeuException(String n){
		super(n);
	}
}
