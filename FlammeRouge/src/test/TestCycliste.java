package test;
import static libtest.Lanceur.lancer;
import static libtest.OutilTest.*;

import java.util.*;

import joueur.Sprinter;

import circuit.*;
import cartes.Carte;

/**
 * Classe permettant de tester la classe cycliste
 * @author totor
 *
 */
public class TestCycliste {
	
	/**
	 *Test des deplacement simple d'un cycliste avec des cartes 
	 */
	public void test_01_cyclistetestIncrementationSimple(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		//assertions
		assertEquals("Il ne devrait pas avoir passé la ligne",false,cyc.aPasseLaLigne());
		assertEquals("Il devrait etre sur la case de depart",0,cyc.getCaseTuile());
		assertEquals("Il devrait être sur la tuile de départ",0,cyc.getPositionActuelle());
		//instructions de test
		Carte car = new Carte(2);
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  2 case ",2,cyc.getCaseTuile());
		assertEquals("Il devrait être sur la tuile de départ",0,cyc.getPositionActuelle());
		assertEquals("Il devrait y avoir un coureur sur la case 2",1,circ.get(0).getCasesOccues()[1]);
		//instructions de test
		car = new Carte(3);
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la 5 case ",5,cyc.getCaseTuile());
		assertEquals("Il devrait être sur la tuile de départ",0,cyc.getPositionActuelle());
		assertEquals("Il devrait en pas y avoir un coureur sur la case 2",0,circ.get(0).getCasesOccues()[1]);
		assertEquals("Il devrait y avoir un coureur sur la case 5",1,circ.get(0).getCasesOccues()[4]);
		//instructions de test
		car = new Carte(3);
		cyc.fairemouvement(car);
		//asseertions
		assertEquals("Il devrait etre sur la 3 case ",3,cyc.getCaseTuile());
		assertEquals("Il devrait être sur la 2 eme Tuile",1,cyc.getPositionActuelle());
		//instructions de test
		car = new Carte(6);
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la 5 case ",4,cyc.getCaseTuile());
		assertEquals("Il devrait être sur la tuile de départ",2,cyc.getPositionActuelle());
	}
	
	/**
	 * Permet de tester la bonne incrementation de plusieurs cyclistes en meme temps
	 */
	public void test_02_cyclistetestIncrementationplusieurs(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Sprinter cyc3 = new Sprinter("violet",c);
		Carte car = new Carte(2);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  2 case ",2,cyc.getCaseTuile());
		assertEquals("Il devrait etre sur la  2 case ",2,cyc2.getCaseTuile());
		assertEquals("Il devrait être sur la tuile de départ",0,cyc.getPositionActuelle());
		assertEquals("Il devrait être sur la tuile de départ",0,cyc2.getPositionActuelle());
		assertEquals("Il devrait y avoir 2 coureur sur la case 2",2,circ.get(0).getCasesOccues()[1]);
		assertEquals("Il devrait être sur la file de droite",true,cyc.isFileDroite());
		assertEquals("Il devrait être sur la file de droite",false,cyc2.isFileDroite());
		cyc3.fairemouvement(car);
		assertEquals("Il devrait etre sur la  1 case ",1,cyc3.getCaseTuile());	
		assertEquals("Il devrait être sur la file de droite",true,cyc3.isFileDroite());
	}
	
	/**
	 * Permet de verifier le bon fonctionnement de la methode a passe la ligne
	 */
	public void test_03_aPasseLaLigne(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(6);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait avoir passé la ligne!",true,cyc.aPasseLaLigne());
	}
	
	/**
	 *Permet de verifier le bon fonctionnement de la descente avec un cycliste 
	 */
	public void test_04_testDescente(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,false,false);
		t1.setDescente(true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(2);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  5 cases ",5,cyc.getCaseTuile());	
	}
	
	
	
	/**
	 * Permet de tester le comportement d'un cycliste lorsqu'il part d'une tuile monte
	 */
	public void test_05_testMonte(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		t1.setMonte(true);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(6);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  5 cases ",5,cyc.getCaseTuile());	
	}
	
	/**
	 * Permet de verifier le bon comportement d'un cycliste lors de son arrive / depart d'une case monte
	 */
	public void test_06_testMontePlusComplexe(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,true);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		t2.setMonte(true);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(8);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  1er case ",1,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 2 eme tuile",1,cyc.getPositionActuelle());
		//instructions de test
		Carte car2 = new Carte(6);
		cyc.fairemouvement(car2);
		//assertions
		assertEquals("Il devrait etre sur la  1er case ",1,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 3 eme tuile",2,cyc.getPositionActuelle());
		//instructions de test
		Carte car3 = new Carte(4);
		cyc.fairemouvement(car3);
		//assertions
		assertEquals("Il devrait etre sur la  5eme case ",5,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 3 eme tuile",2,cyc.getPositionActuelle());
	}

	/**
	 * Test vérifiant le bon comportement dans une conditions normal des montées
	 */
	public void test_07_test_supplémentaire_monte(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		t1.setMonte(true);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(3);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  3eme case ",3,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 1er tuile",0,cyc.getPositionActuelle());
	}
	
	/**
	 * Permet de verifier que les cyclistes ne sort pas du tableau avec un null pointeur
	 */
	public void test_08_avancer(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		t1.setMonte(true);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Carte car = new Carte(3);
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  3er case ",3,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc.getPositionActuelle());
		assertEquals("Il devrait avoir passé 3 cases",3,cyc.getCasePasse());
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  5eme case ",5,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc.getPositionActuelle());
		assertEquals("Il devrait avoir passé 5",5,cyc.getCasePasse());
		//instructions de test
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  5eme case ",5,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc.getPositionActuelle());
		assertEquals("Il devrait avoir passé 5",5,cyc.getCasePasse());
	}
	
	/**
	 * Permet de verifier la bonne gestion  des avancements avec les files
	 */
	public void test_09_testBisDeChangementDeLigne(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		t1.setMonte(true);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("bleu",c);
		Carte car = new Carte(3);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  3er case ",3,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc.getPositionActuelle());
		assertEquals("Il devrait etre sur la  3er case ",3,cyc2.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc2.getPositionActuelle());
		assertEquals("Le premier devrait être à droite",true,cyc.isFileDroite());
		assertEquals("Le deuxiement devrait être à gauche",false,cyc2.isFileDroite());
		//instructions de test
		Sprinter cyc3 = new Sprinter("bleu",c);
		cyc3.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  3er case ",2,cyc3.getCaseTuile());	
		assertEquals("Il devrait être sur la 1 er tuile",0,cyc3.getPositionActuelle());
		assertEquals("Le troixieme devrait être à droite",true,cyc3.isFileDroite());
	}
	
	/**
	 * Permet de tester la gestion du nombre de cases passe
	 */
	public void test_10_testCasesPasses(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		//instructions de test
		Carte car = new Carte(8);
		cyc.fairemouvement(car);
		//assertions
		assertEquals("Il devrait etre sur la  3eme case ",3,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 2 eme tuile",1,cyc.getPositionActuelle());
		assertEquals("Le troixieme devrait être à droite",true,cyc.isFileDroite());
		assertEquals("Le cycliste aurait du avancer de 8 cases",8,cyc.getCasePasse());
		//instructions de test
		cyc.fairemouvement(new Carte(4));
		//assertions
		assertEquals("Il devrait etre sur la  3eme case ",2,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la dernière tuile",2,cyc.getPositionActuelle());
		assertEquals("Le cycliste aurait du avancer de 12",12,cyc.getCasePasse());
		//instructions de test
		cyc.fairemouvement(new Carte (6));
		//assertions
		assertEquals("Il devrait etre sur la  5eme case ",5,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la 2 eme tuile",2,cyc.getPositionActuelle());
		assertEquals("Le troixieme devrait être à droite",true,cyc.isFileDroite());
		assertEquals("Le cycliste aurait ud avancer de 15",15,cyc.getCasePasse());
	}
	
	/**
	 * Permet de vérifier la bonne gestion des case avancée pour un cycliste
	 */
	public void test_11_casePasseTestComplexe(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Sprinter cyc = new Sprinter("bleu",c);
		//instructions de test
		cyc.fairemouvement(new Carte(14));
		//assertions
		assertEquals("Il devrait etre sur la  4eme case ",4,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la dernière tuile",2,cyc.getPositionActuelle());
		assertEquals("Le troixieme devrait être à droite",true,cyc.isFileDroite());
		assertEquals("Le cycliste aurait du avancer de 15",14,cyc.getCasePasse());
		//instructions de test
		cyc.fairemouvement(new Carte(5));
		//assertions
		assertEquals("Il devrait etre sur la  4eme case ",5,cyc.getCaseTuile());	
		assertEquals("Il devrait être sur la dernière tuile tuile",2,cyc.getPositionActuelle());
		assertEquals("Le troixieme devrait être à droite",true,cyc.isFileDroite());
		assertEquals("Le cycliste aurait du avancer de 15",15,cyc.getCasePasse());
	}
	
	
	
	/**
	 * Methode pour lancer les tests de la classe cycliste
	 * @param args
	 */
	public static void main(String args[])
	{
		lancer(new TestCycliste(),args);

	}

}
