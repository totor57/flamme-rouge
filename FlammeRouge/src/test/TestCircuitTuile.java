package test;
import static libtest.OutilTest.*;

import java.util.*;
import java.io.*;

import circuit.Circuit;
import circuit.Tuile;
import circuit.TuileLigneDroite;
import circuit.TuileVirage;

import static libtest.Lanceur.*;

/**
 * Classe permetant de test la classe circuit et tuile
 * @author totor & blondin
 *
 */
public class TestCircuitTuile {

	/**
	 * Permet de verifier le bon fonctionnement du constructeur de circuit de facon direct
	 */
	public void test_01_construcTuile(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(4,false,false);
		//assertions
		assertEquals("la tuile devrait avoir 4 cases", 4, t1.getNbCases());
		TuileVirage t2 = new TuileVirage("leger",false);
		assertEquals("La tuile devrait avoir 3 cases",4,t2.getNbCases());
		assertEquals("La tuile devrait être leger",true,t2.estLege());
		assertEquals("La tuilde ne devrait pas etre serre",false,t2.estSerre());
		t2 = new TuileVirage("serre",false);
		assertEquals("La tuilde devrait etre serre",true,t2.estSerre());
		assertEquals("La tuile ne devrait pas être leger",false,t2.estLege());
	}
	
	/**
	 * Permet de verifier le bon fonctionnement du constructeur de circuit de facon indirect
	 * @throws IOException si jamais le fichier n'a pas ete trouve
	 */
	public void test_02_constructeurCircuit() throws IOException{
		//Jeu de données
		Circuit retour =null;
		try{
			retour = Circuit.setCircuit("circuitTest");
		}
		catch(IOException e){
			throw new IOException();
		}
		//assertions
		//Devrait retourner un circuit de type : départ, Ligne droite, Virage serré, virage légé, tout droit,arrivé
		List<Tuile> temp = retour.getListTuile();
		assertEquals("Le circuit devrait avoir 5 tuiles",6,temp.size());
		assertEquals("La premiere tuile devrait être le depart",true,((TuileLigneDroite)temp.get(0)).estDepart());
		assertEquals("La deuxieme tuile ne devrait pas être le depart",false,((TuileLigneDroite)temp.get(1)).estDepart());
		assertEquals("La deuxieme tuile ne devrait pas être l'arrivé",false,((TuileLigneDroite)temp.get(1)).estArrive());
		assertEquals("La troisieme tuile devrait etre un virage serré",true,((TuileVirage)temp.get(2)).estSerre());
		assertEquals("La quatrième tuile devrait etre un virage légé",true,((TuileVirage)temp.get(3)).estLege());
		assertEquals("La cinquième tuile ne devrait pas être le depart",false,((TuileLigneDroite)temp.get(4)).estDepart());
		assertEquals("La cinquième tuile ne devrait pas être l'arrivé",false,((TuileLigneDroite)temp.get(4)).estArrive());
		assertEquals("La denière tuile devrait être l'arrivé",true,((TuileLigneDroite)temp.get(5)).estArrive());
	}
	/**
	 * Methode peremettant de lancer les tests
	 * @param args
	 */
	public static void main(String args[])
	{
		lancer(new TestCircuitTuile(),args);

	}
}
