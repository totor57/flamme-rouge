package test;
import circuit.*;
import static libtest.Lanceur.lancer;
import static libtest.OutilTest.*;

import java.util.*;

import joueur.*;
import cartes.Carte;


/**
 * Classe de test de la classe joueur 
 * @author totor
 *
 */
public class TestJoueur {
	
	/**
	 * Test du constructeur de joueur
	 */
	public void test_01_construcJoueur(){
		//Jeu de données
		Circuit c = new Circuit("dsq",new ArrayList<Tuile>());
		Joueur j = new Joueur("test","bleu",c);
		//assertions
		assertEquals("Le joueur devrait s'appeller test","test",j.getNom());
		assertEquals("Le joueur devrait avoir la couleur bleu","bleu",j.getCouleur());
		//instructions de test
		List<Cycliste> lc = j.getListCycliste();
		//assertions
		assertEquals("Le premier cycliste devrait etre un rouleur",true,lc.get(0) instanceof Rouleur);
		assertEquals("Le deuxieme cycliste devrait etre un sprinteur",true,lc.get(1) instanceof Sprinter);
		
	}
	
	/**
	 * Verification de la bonne initialisation des cartes des joueurs 
	 */
	public void test_02_initCartes(){
		//Jeu de données
		Circuit c = new Circuit("dsq",new ArrayList<Tuile>());
		Joueur j = new Joueur("test","bleu",c);
		//instructions de test
		List<Carte> l1 = j.getListeCartesDefaussesRouleur();
		List<Carte> l2 = j.getListeCartesDefaussesSprinteur();
		//assertions
		assertEquals("La défausse rouleur devrait être vide",0,l1.size());
		assertEquals("La défausse sprinteur devrait être vide",0,l2.size());
		//instructions de test
		l1 = j.getListeCarteCachRouleur();
		l2 = j.getListeCarteCachSprinteur();
		//assertions
		assertEquals("La pioche rouleur devrait avoir 15 cartes",15,l1.size());
		assertEquals("La pioche sprinteur devrait avoir 15 cartes",15,l2.size());
		assertEquals("La carte la plus faible des rouleurs devrait etre 3",3,l1.get(0).getNumero());
		assertEquals("La carte la plus faible des sprinteur devrait etre 2",2,l2.get(0).getNumero());
		assertEquals("La carte la plus forte des rouleurs devrait etre 7",7,l1.get(14).getNumero());
		assertEquals("La carte la plus forte des sprinteur devrait etre 9",9,l2.get(14).getNumero());
		
	}
	
	/**
	 * Verification du bon fonctionnement de la pioche avec assez de cartes dans la pioche
	 */
	public void test_03_testPiocheSimple(){
		//Jeu données
		Circuit c = new Circuit("dsq",new ArrayList<Tuile>());
		Joueur j = new Joueur("test","bleu",c);
		//instructions de test
		j.piocher3Cartes(1);
		j.piocher3Cartes(2);
		List<Carte> l1 = j.getListeCarteCachSprinteur();
		List<Carte> l2 = j.getListeCarteCachRouleur();
		//assertions
		assertEquals("Le paquet du joueur devrait maintenant aviur une taille de 12",12,l1.size());
		assertEquals("Le paquet du joueur devrait maintenant aviur une taille de 12",12,l2.size());
	}
	
	/**
	 * Verification du bon fonctionnement de la pioche lorsque la pioche ne contiend pas assez de cartes 
	 */
	public void test_04_testPiocheAvecPlusAssezDeCarte(){
		//Jeu de données
		Circuit circ = new Circuit("dsq",new ArrayList<Tuile>());
		Joueur j = new Joueur("test","bleu",circ);
		//instructions de test
		j.setListeCartesDefaussesSprinteur(j.getListeCarteCachSprinteur());
		j.setListeCartesDefaussesRouleur(j.getListeCarteCachRouleur());
		j.setListeCarteCachRouleur(new ArrayList<Carte>());
		j.setListeCarteCachSprinteur(new ArrayList<Carte>());
		//assertions
		assertEquals("Le pioche rouleur devrait être vide",0,j.getListeCarteCachRouleur().size());
		assertEquals("Le pioche sprinteur devrait être vide",0,j.getListeCarteCachSprinteur().size());
		//instructions de test
		Carte[] c = j.piocher3Cartes(1);
		j.piocher3Cartes(2);
		//assertions
		assertEquals("Le paquet du joueur devrait maintenant aviur une taille de 12",12,j.getListeCarteCachRouleur().size());
		assertEquals("Le paquet du joueur devrait maintenant aviur une taille de 12",12,j.getListeCarteCachSprinteur().size());
		assertEquals("La défausse du joueur devrait maintenant aviur une taille de 0",0,j.getListeCartesDefaussesSprinteur().size());
	}

	/**
	 * Permet de lancer tous les tests de joueur
	 * @param args
	 */
	public static void main(String args[])
	{
		lancer(new TestJoueur(),args);

	}
}
