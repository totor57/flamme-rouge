package test;
import static libtest.Lanceur.lancer;
import java.io.*;
import static libtest.OutilTest.*;

import java.util.*;

import principale.Jeu;

import joueur.*;
import circuit.*;
import cartes.Carte;

/**
 * Classe de test permettant de tester la classe jeu
 * @author totor
 *
 */
public class TestJeu {
	
	/**
	 * Verifie le bon fonctionnement du constructeur de jeu
	 */
	public void test_01_construcJeu(){
		//Jeu de données
		List<Tuile>  ls = new ArrayList<Tuile>();
		ls.add(new TuileLigneDroite(10,false,false));
		Circuit c = new Circuit("test",ls);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(new Joueur("cc","cc",c));
		Jeu jc = new Jeu(c,lj,false);
		//assertions
		assertEquals("La taille du tableau représentant les coureurs devrai être de 10",14,jc.getTabPosCycliste().length);
		//instructions de test
		ls.add(new TuileLigneDroite(20,false,false));
		c = new Circuit("test",ls);
		jc = new Jeu(c,lj,false);
		//assertions
		assertEquals("La taille du tableau représentant les coureurs devrai être de 10",34,jc.getTabPosCycliste().length);
	}
	
	/**
	 * Verifie le bon fonctionnement de l'aspiration avec 2 cyclistes
	 */
	public void test_02_testAspirationsSimple(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		Carte car = new Carte(3);
		Carte car2 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		//assertions
		assertEquals("Le cycliste 1 devrait être sur la 3eme case",3,cyc.getCaseTuile());
		assertEquals("Le cycliste 2 devrait être sur la 2",1,cyc2.getCaseTuile());
		//instructions de test
		jc.dernierePhase();
		//assertions
		assertEquals("Le cycliste 1 devrait toujours être sur la 3eme case",3,cyc.getCaseTuile());
		assertEquals("La case du cycliste devrait être",cyc,jc.getTabPosCycliste()[6][0]);
		assertEquals("Le cycliste 2 devrait avoir effectué une aspiration et être sur la 2",2,cyc2.getCaseTuile());
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[4][0]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc2,jc.getTabPosCycliste()[5][0]);
	}
	
	/**
	 * Verifie le bon fonctionnement de l'aspiratino avec 2 cyclistes
	 */
	public void test_03_testAspirationPlus(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		Joueur j3 = new Joueur("j3","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		lj.add(j3);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Sprinter cyc3 = new Sprinter("purple",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		List<Cycliste> lsCyc3 = new ArrayList<Cycliste>();
		lsCyc3.add(cyc3);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		j3.setListCycliste(lsCyc3);
		Carte car = new Carte(3);
		Carte car2 = new Carte(1);
		Carte car3 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		cyc3.fairemouvement(car3);
		//assertions
		assertEquals("Le cycliste 1 devrait être sur la 3eme case",3,cyc.getCaseTuile());
		assertEquals("Le cycliste 2 devrait être sur la 1er case",1,cyc2.getCaseTuile());
		assertEquals("Le cycliste 3 devrait être sur la 1er case",1,cyc2.getCaseTuile());
		//instructions de test
		jc.dernierePhase();
		//assertions
		assertEquals("Le cycliste 1 devrait toujours être sur la 3eme case",3,cyc.getCaseTuile());
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste 2 devrait être null",cyc,jc.getTabPosCycliste()[6][0]);
		assertEquals("Le cycliste 2 devrait avoir effectué une aspiration et être sur la 2",2,cyc2.getCaseTuile());
		assertEquals("Le cycliste 3 devrait avoir effectué une aspiration et être sur la 2",2,cyc3.getCaseTuile());
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[4][0]);
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[4][1]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc2,jc.getTabPosCycliste()[5][0]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc3,jc.getTabPosCycliste()[5][1]);
	}
	
	/**
	 * Verifie le bon fonctionnement de l'aspiration avec un groupe de cycliste 
	 */
	public void test_04_aspirationAvecGroupe(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		Joueur j3 = new Joueur("j3","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		lj.add(j3);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Sprinter cyc3 = new Sprinter("purple",c);
		Rouleur cyc4 = new Rouleur("bleu",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		lsCyc.add(cyc4);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		List<Cycliste> lsCyc3 = new ArrayList<Cycliste>();
		lsCyc3.add(cyc3);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		j3.setListCycliste(lsCyc3);
		Carte car = new Carte(4);
		Carte car2 = new Carte(2);
		Carte car3 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		cyc3.fairemouvement(car2);
		cyc4.fairemouvement(car3);
		//assertions
		assertEquals("Le cycliste 1 devrait être sur la 4eme case",4,cyc.getCaseTuile());
		assertEquals("Le cycliste 2 devrait être sur la 2er case",2,cyc2.getCaseTuile());
		assertEquals("Le cycliste 3 devrait être sur la 2er case",2,cyc2.getCaseTuile());
		assertEquals("Le cycliste 4 devrait être sur la 1er case",1,cyc4.getCaseTuile());
		//instructions de test
		jc.dernierePhase();
		//assertions
		assertEquals("Le cycliste 1 devrait toujours être sur la 3eme case",4,cyc.getCaseTuile());
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste 2 devrait être null",cyc,jc.getTabPosCycliste()[7][0]);
		assertEquals("Le cycliste 2 devrait avoir effectué une aspiration et être sur la 2",3,cyc2.getCaseTuile());
		assertEquals("Le cycliste 3 devrait avoir effectué une aspiration et être sur la 2",3,cyc3.getCaseTuile());
		assertEquals("Le cycliste 4 devrait avoir effectué une aspiration et être sur la 2",2,cyc4.getCaseTuile());
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[4][0]);
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[4][1]);
		assertEquals("L'ancienne case du tableau sur laquelle était le cycliste devrait être null",null,jc.getTabPosCycliste()[3][0]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc2,jc.getTabPosCycliste()[6][0]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc3,jc.getTabPosCycliste()[6][1]);
		assertEquals("La nouvelle carte correspond à l'autre",cyc4,jc.getTabPosCycliste()[5][0]);
	}
	
	/**
	 * Permet de verifier le bon fonctionnement du malus des cartes fatigues
	 */
	public void test_05_testCarteFatigue(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		Carte car = new Carte(3);
		Carte car2 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		//assertions
		assertEquals("La pioche de carte fatigue sprinteur devrait être de 30:",30,jc.getCarteFatigueSprinteur().size());
		//instructions de test
		jc.dernierePhase();
		//assertions
		assertEquals("La pioche de carte fatigue sprinteur devrait être de 29:",29,jc.getCarteFatigueSprinteur().size());
		
	}
	
	/**
	 * Permet de verifier les bons positionnement des cyclistes dans le classement
	 */
	public void test_06_testClassement(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Rouleur cyc3 = new Rouleur("bleu",c);
		Rouleur cyc4 = new Rouleur("vert",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		lsCyc.add(cyc3);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		lsCyc2.add(cyc4);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		Carte car = new Carte(3);
		Carte car2 = new Carte(1);
		Carte car3 = new Carte(6);
		Carte car4 = new Carte(5);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		cyc3.fairemouvement(car4);
		cyc4.fairemouvement(car3);
		List<Cycliste> classement = jc.classementCycliste();
		//assertions
		assertEquals("Le 1er devrait être le cycliste 3",cyc4,classement.get(0));
		assertEquals("Le 2eme devrait être le cycliste 4",cyc3,classement.get(1));
		assertEquals("Le 3 eme devrait être le cycliste 1",cyc,classement.get(2));
		assertEquals("Le 4 eme devrait être le cycliste 2",cyc2,classement.get(3));
	}
	
	/**
	 * Permet de verifier le bon fonctionnement de l'écriture du dernier classement (a faire visuellement)
	 */
	public void test_07_testSortiClassement(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		Joueur j3 = new Joueur("j3","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		lj.add(j3);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Sprinter cyc3 = new Sprinter("purple",c);
		Rouleur cyc4 = new Rouleur("bleu",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		lsCyc.add(cyc4);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		List<Cycliste> lsCyc3 = new ArrayList<Cycliste>();
		lsCyc3.add(cyc3);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		j3.setListCycliste(lsCyc3);
		Carte car = new Carte(4);
		Carte car2 = new Carte(2);
		Carte car3 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		cyc3.fairemouvement(car2);
		cyc4.fairemouvement(car3);
		jc.ecrirerLeClassementDansFichier();
	}
	
	/**
	 * Permet de verifier le bon fonctionnement de la sauvegarde d'un jeu
	 */
	public void test_08_testSauvegardeJeu(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		Joueur j3 = new Joueur("j3","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		lj.add(j3);
		Jeu jc = new Jeu(c,lj,false);
		
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Sprinter cyc3 = new Sprinter("purple",c);
		Rouleur cyc4 = new Rouleur("bleu",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		lsCyc.add(cyc4);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		List<Cycliste> lsCyc3 = new ArrayList<Cycliste>();
		lsCyc3.add(cyc3);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		j3.setListCycliste(lsCyc3);
		Carte car = new Carte(4);
		Carte car2 = new Carte(2);
		Carte car3 = new Carte(1);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car2);
		cyc3.fairemouvement(car2);
		cyc4.fairemouvement(car3);
		jc.sauvegarder("save1");
		Jeu nouveauJeu = null;
		try{
			nouveauJeu = Jeu.charcherSauvegarde("save1");
		}
		catch(IOException e){
			System.out.println("Houston on a un problème");
		}
		//assertions
		assertEquals("Le jeu devrait avoir 3 tuiles ",3,nouveauJeu.getPiste().getListTuile().size());
		assertEquals("Le jeu devrait avoir 3 joueurs",3,nouveauJeu.getListeJ().size());
	}
	
	/**
	 * Test permettant de verifier la bonne concordance du tableau representant le jeu et les positions des cyclistes
	 */
	public void test_09_incrementationEtrange(){
		//Jeu de données
		TuileLigneDroite t1 = new TuileLigneDroite(5,true,false);
		TuileLigneDroite t2 = new TuileLigneDroite(5,false,false);
		TuileLigneDroite t3 = new TuileLigneDroite(5,false,true);
		List<Tuile> circ = new ArrayList<Tuile>();
		circ.add(t1);
		circ.add(t2);
		circ.add(t3);
		Circuit c = new Circuit("test",circ);
		Joueur j1 = new Joueur("j1","bleu",c);
		Joueur j2 = new Joueur("j2","cc",c);
		List<Joueur> lj = new ArrayList<Joueur>();
		lj.add(j1);
		lj.add(j2);
		Jeu jc = new Jeu(c,lj,false);
		Sprinter cyc = new Sprinter("bleu",c);
		Sprinter cyc2 = new Sprinter("vert",c);
		Rouleur cyc4 = new Rouleur("bleu",c);
		List<Cycliste> lsCyc = new ArrayList<Cycliste>();
		lsCyc.add(cyc);
		lsCyc.add(cyc4);
		List<Cycliste> lsCyc2 = new ArrayList<Cycliste>();
		lsCyc2.add(cyc2);
		j1.setListCycliste(lsCyc);
		j2.setListCycliste(lsCyc2);
		Carte car = new Carte(4);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car);
		jc.dernierePhase();
		//assertions
		assertEquals("Il a bougé vers la case 4 (cyc)",cyc,jc.getTabPosCycliste()[7][0]);
		assertEquals("Il a bougé vers la case 4 (cyc2)",cyc2,jc.getTabPosCycliste()[7][1]);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car);
		jc.dernierePhase();
		//assertions
		assertEquals("Il a bougé vers la case 8 (cyc2)",cyc2,jc.getTabPosCycliste()[11][1]);
		assertEquals("Il a bougé vers la case 8 (cyc)",cyc,jc.getTabPosCycliste()[11][0]);
		//instructions de test
		cyc.fairemouvement(car);
		cyc2.fairemouvement(car);
		jc.dernierePhase();
		//assertions
		assertEquals("Il a bougé vers la case 12 (cyc2)",cyc2,jc.getTabPosCycliste()[15][1]);
		assertEquals("Il a bougé vers la case 12 (cyc)",cyc,jc.getTabPosCycliste()[15][0]);
	}
	
	/**
	 * Méthode permettant de tester la classe jeu
	 * @param args
	 */
	public static void main(String args[])
	{
		lancer(new TestJeu(),args);

	}
}
