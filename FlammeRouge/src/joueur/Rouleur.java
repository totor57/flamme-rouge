package joueur;

import circuit.*;
import java.io.Serializable;


/**
 * Classe representant un rouleur
 * 
 * @author totor & blondin
 *
 */
public class Rouleur extends Cycliste implements Serializable{

	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur d'un rouleur avec une couleur
	 * @param c couleur du rouleur
	 */
	public Rouleur(String c,Circuit circCourant){
		super(c,circCourant);
	}
	
	/**
	 * Permet d'afficher un rouleur avec un print
	 */
	public String toString(){
		return ("Le rouleur "+super.toString());
	}
}
