package joueur;

import circuit.*;
import java.io.Serializable;
import java.awt.Color;
import java.util.*;

import cartes.Carte;



/**
 * Classe representant un joueur
 * @author totor & bondin
 */
public class Joueur implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nom du joueur
	 */
	private String nom;
	
	/**
	 * couleur du joueur
	 */
	private String couleur;
	
	/**
	 * Liste des cyclistes possédés par le joueur
	 */
	private List<Cycliste> listCycliste;
	
	/**
	 * Associe au sprinteur et au rouleur (1,2) du joueur la carte qu'il a choisi de jouer 
	 */
	private Hashtable<Integer,Carte> carteAjouer;
	
	/**
	 * Liste des cartes face cache (deck) sprinteur du joueur (n'est pas melange au debut mais seulement lors de la pioche)
	 */
	private List<Carte> listeCarteCachSprinteur;
	
	/**
	 * Liste des cartes face cache (deck) rouleur du joueur 
	 */
	private List<Carte> listeCarteCachRouleur;
	
	/**
	 * Liste des cartes de la defausse sprinteur du joueur 
	 */
	private List<Carte> listeCartesDefaussesSprinteur;
	
	/**
	 * Liste des cartes de la defausse rouleur du joueur 
	 */
	private List<Carte> listeCartesDefaussesRouleur;
	
	
	/**
	 * Couleur du joueur (uniquement utilise pour l'affichage graphique) 
	 */
	private Color couleurGraph;
	
	/**
	 * Constructeur non graphique de joueur
	 * @param n nom du joueur 
	 * @param c couleur non graphique
	 */
	public Joueur(String n,String c,Circuit circCourant){
		this.nom=n;
		this.couleur=c;
		this.carteAjouer = new Hashtable<Integer,Carte>();
		this.carteAjouer.put(new Integer(1), new Carte(-1));
		this.carteAjouer.put(new Integer(2), new Carte(-1));
		this.listCycliste = new ArrayList<Cycliste>();
		listCycliste.add(new Rouleur(this.couleur,circCourant));
		listCycliste.add(new Sprinter(this.couleur,circCourant));
		this.listeCartesDefaussesSprinteur = new ArrayList<Carte>();
		this.listeCartesDefaussesRouleur = new ArrayList<Carte>();
		this.listeCarteCachSprinteur = new ArrayList<Carte>();
		this.listeCarteCachRouleur = new ArrayList<Carte>();
		for(int i = 0;i<15;i++){
			if(i<3){
				this.listeCarteCachRouleur.add(new Carte(3));
				this.listeCarteCachSprinteur.add(new Carte(2));
			}
			else if(i<6){
				this.listeCarteCachRouleur.add(new Carte(4));
				this.listeCarteCachSprinteur.add(new Carte(3));
			}
			else if(i<9){
				this.listeCarteCachRouleur.add(new Carte(5));
				this.listeCarteCachSprinteur.add(new Carte(4));
			}
			else if(i<12){
				this.listeCarteCachRouleur.add(new Carte(6));
				this.listeCarteCachSprinteur.add(new Carte(5));
			}
			else{
				this.listeCarteCachRouleur.add(new Carte(7));
				this.listeCarteCachSprinteur.add(new Carte(9));
			}
		}
	}
	
	/**
	 * Constructeur de joueur en mode graphique 
	 * @param n nom du joueur
	 * @param c couleur non graphique
	 * @param coul couleur graphique
	 */
	public Joueur(String n,String c,Color coul,Circuit circCourant){
		this.nom=n;
		this.couleur=c;
		this.carteAjouer = new Hashtable<Integer,Carte>();
		this.carteAjouer.put(new Integer(1), new Carte(-1));
		this.carteAjouer.put(new Integer(2), new Carte(-1));
		this.listCycliste = new ArrayList<Cycliste>();
		listCycliste.add(new Rouleur(this.couleur,circCourant));
		listCycliste.add(new Sprinter(this.couleur,circCourant));
		this.listeCartesDefaussesSprinteur = new ArrayList<Carte>();
		this.listeCartesDefaussesRouleur = new ArrayList<Carte>();
		this.listeCarteCachSprinteur = new ArrayList<Carte>();
		this.listeCarteCachRouleur = new ArrayList<Carte>();
		for(int i = 0;i<15;i++){
			if(i<3){
				this.listeCarteCachRouleur.add(new Carte(3));
				this.listeCarteCachSprinteur.add(new Carte(2));
			}
			else if(i<6){
				this.listeCarteCachRouleur.add(new Carte(4));
				this.listeCarteCachSprinteur.add(new Carte(3));
			}
			else if(i<9){
				this.listeCarteCachRouleur.add(new Carte(5));
				this.listeCarteCachSprinteur.add(new Carte(4));
			}
			else if(i<12){
				this.listeCarteCachRouleur.add(new Carte(6));
				this.listeCarteCachSprinteur.add(new Carte(5));
			}
			else{
				this.listeCarteCachRouleur.add(new Carte(7));
				this.listeCarteCachSprinteur.add(new Carte(9));
			}
		}
		this.couleurGraph=coul;
	}	
	
	/**
	 * Permet pour un joueur de lui faire piocher et choisir ces 2 cartes de mouvements pour son sprinteur et son rouleur
	 */
	public void piocher(){
		String n=Interation.joueur_piocher1(); 
		Carte[] tab = new Carte[3];
		if(n.equals("1")){
			tab=piocher3Cartes(1);
			choisirParmiLes3(tab,1);
			tab=piocher3Cartes(2);
			choisirParmiLes3(tab,2);
		}
		else{
			tab=piocher3Cartes(2);
			choisirParmiLes3(tab,2);
			tab=piocher3Cartes(1);
			choisirParmiLes3(tab,1);
		}
	}
	
	/**
	 * Permet de piocher 3 cartes de facon aleatoire parmis la pioche du joueur qui peut être fusionnée avec la défausse en cas de necessite ainsi que de mettre a jour les pioches
	 * @param choix qui vaut 1 si le joueur voulait piocher pour son prinsteur 2 sinon
	 * @return un tableau contenant les 3 cartes pioché
	 */
	public Carte[] piocher3Cartes(int choix){
		Carte[] tab = new Carte[3];
		List<Carte> liste = null;
		int indice = 0;
		if (choix == 1){
			if(this.listeCarteCachSprinteur.size()<3){
				this.listeCarteCachSprinteur.addAll(this.listeCartesDefaussesSprinteur);
				this.listeCartesDefaussesSprinteur=new ArrayList<Carte>();
			}
			liste =this.listeCarteCachSprinteur;
		}
		else{
			if(this.listeCarteCachRouleur.size()<3){
				this.listeCarteCachRouleur.addAll(this.listeCartesDefaussesRouleur);
				this.listeCartesDefaussesRouleur=new ArrayList<Carte>();
			}
			liste =this.listeCarteCachRouleur;
		}
		for(int i = 0;i<3;i++){
			indice = (int)(Math.random()*liste.size());
			tab[i]=liste.get(indice);
			liste.remove(indice);
		}
		return tab;
	}
	
	/**
	 * Permet au joueur de choisir parmi les 3 cartes pioches 
	 * @param tab cartes qui ont été pioches
	 * @param choixCoureur vaut 1 si c'est pour son sprinteur sinon 2
	 */
	public void choisirParmiLes3(Carte[] tab,int choixCoureur){
		Interation.joueur_choisir3cartes(choixCoureur);
		for(int i =0;i<3;i++){
			Interation.joueur_choisir3cartes2(i,tab[i]);
		}
		int choix = Interation.joueur_choisir3cartes3();
		if(choixCoureur == 1){
			this.carteAjouer.put(new Integer(1), tab[choix-1]);
			for(int i = 0;i<3;i++){
				if(i!=choix-1) this.listeCartesDefaussesSprinteur.add(tab[i]);
			}
		}
		else{
			this.carteAjouer.put(new Integer(2), tab[choix-1]);
			for(int i = 0; i <3;i++){
				if(i!=choix-1) this.listeCartesDefaussesRouleur.add(tab[i]);
			}
		}
	}
	
	/**
	 * Permet d'ajouter une carte fatigue a la defause rouleur du joueur
	 * @param n carte fatigue qu'on ajoute
	 */
	public void recoitMalusR(Carte n){
		this.listeCarteCachRouleur.add(n);
	}
	
	/**
	 * Permet d'ajouter une carte fatigue a la defause sprinteur du joueur
	 * @param n carte fatigue qu'on ajoute
	 */
	public void recoitMalusS(Carte n){
		this.listeCarteCachSprinteur.add(n);
	}
	

	/**
	 * Getteur du nom du joueur
	 * @return nom du joueur
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * Permet de modifier le nom du joueur 
	 * @param nom nouveau nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	 * Permet d'obtenir la couleur du joueur
	 * @return couleur du joueur
	 */
	public String getCouleur() {
		return couleur;
	}

	/**
	 * Permet de modifier la couleur du joueur
	 * @param couleur nouvelle couleur
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	/**
	 * Permet d'obtenir la liste des cyclistes
	 * @return listes des cyclistes du joueur
	 */
	public List<Cycliste> getListCycliste() {
		return listCycliste;
	}

	/**
	 * Permet de modifier la liste des cyclistes
	 * @param listCycliste nouvelle liste de cycliste
	 */
	public void setListCycliste(List<Cycliste> listCycliste) {
		this.listCycliste = listCycliste;
	}

	/**
	 * Permet d'obtenir le deck sprinteur
	 * @return deck sprinteur du joueur
	 */
	public List<Carte> getListeCarteCachSprinteur() {
		return listeCarteCachSprinteur;
	}

	/**
	 * Permet de modifier le deck sprinteur
	 * @param listeCarteCachSprinteur nouveau deck sprinteur du joueur
	 */
	public void setListeCarteCachSprinteur(List<Carte> listeCarteCachSprinteur) {
		this.listeCarteCachSprinteur = listeCarteCachSprinteur;
	}

	/**
	 * Permet d'obtenir le deck rouleur
	 * @return deck rouleur du joueur
	 */
	public List<Carte> getListeCarteCachRouleur() {
		return listeCarteCachRouleur;
	}
	
	/**
	 * Permet de modifier le deck rouleur
	 * @param listeCarteCachRouleur nouveau deck rouleur du joueur
	 */
	public void setListeCarteCachRouleur(List<Carte> listeCarteCachRouleur) {
		this.listeCarteCachRouleur = listeCarteCachRouleur;
	}
	
	
	/**
	 * Permet d'obtenir les cartes cache sprinteur
	 * @return carte cache sprinteur du joueur
	 */
	public List<Carte> getListeCartesDefaussesSprinteur() {
		return listeCartesDefaussesSprinteur;
	}

	/**
	 * Permet de modifier les cartes cache sprinteur
	 * @param listeCartesDefausses nouvelles cartes cache sprinteur du joueur
	 */
	public void setListeCartesDefaussesSprinteur(List<Carte> listeCartesDefausses) {
		this.listeCartesDefaussesSprinteur = listeCartesDefausses;
	}
	
	/**
	 * Permet d'obtenir les cartes cache rouleur
	 * @return carte cache rouleur du joueur
	 */
	public List<Carte> getListeCartesDefaussesRouleur() {
		return listeCartesDefaussesRouleur;
	}
	
	/**
	 * Permet de modifier les cartes cache rouleur
	 * @param listeCartes nouvelles cartes cache rouleur du joueur
	 */
	public void setListeCartesDefaussesRouleur(List<Carte> listeCartes) {
		this.listeCartesDefaussesRouleur = listeCartes;
	}

	/**
	 * Permet d'obtenir les cartes que le joueur souhaite jouer
	 * @return les cartes que le joueur va jouer
	 */
	public Hashtable<Integer, Carte> getCarteAjouer() {
		return carteAjouer;
	}

	
	/**
	 * Permet de mofier les cartes que le joueur souhaites jouer
	 * @param carteAjouer nouvelles cartes que le joueur veut jouer
	 */
	public void setCarteAjouer(Hashtable<Integer, Carte> carteAjouer) {
		this.carteAjouer = carteAjouer;
	}
	
	/**
	 * Permet d'obtenir la couleur graphique du joueur
	 * @return couleur graphique du joueur
	 */
	public Color getCouleurGraph(){
		return(this.couleurGraph);
	}
}
