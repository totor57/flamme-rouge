package joueur;

import circuit.*;
import java.io.Serializable;


/**
 * Classe representant un sprinteur
 * 
 * @author totor & blondin
 *
 */
public class Sprinter extends Cycliste implements Serializable{

	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur de sprinteur 
	 * @param c couleur du sprinteur
	 */
	public Sprinter(String c,Circuit circCourant){
		super(c,circCourant);
	}
	
	/**
	 * Permet d'afficher un rouleur avec un print
	 */
	public String toString(){
		return ("Le sprinteur "+super.toString());
	}
}
