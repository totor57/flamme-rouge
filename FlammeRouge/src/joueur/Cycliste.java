package joueur;


import java.io.Serializable;

import cartes.Carte;
import circuit.Circuit;

/**
 * Classe representant un cycliste
 * @author totor & blondin 
 *
 */
public abstract class Cycliste implements Serializable{
	
	/**
	 * id version de serialization
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *Numero de la tuile dans la liste des tuiles du circuit sur laquelle est le cycliste 
	 */
	private int positionActuelle;
	
	/**
	 *Case de la tuile 
	 */
	private int caseTuile;
	/**
	 * Permet de savoir si le cycliste est sur la tuile droite
	 */
	private boolean fileDroite;
	/**
	 * Permet de connaitre la couleur d'un cycliste
	 */
	private String couleur;
	
	/**
	 * nombre de case parcourues par le cycliste!
	 */
	private int casePasse;
	
	/**
	 * Circuit courant sur lequel est le cycliste (dépendance)
	 */
	private Circuit circuitCourant;
	
	/**
	 * Constructeur d'un cycliste
	 * @param c couleur du cycliste 
	 */
	public Cycliste(String c,Circuit circ){
		this.circuitCourant = circ;
		this.caseTuile=0;
		this.positionActuelle=0;
		this.fileDroite=true;
		this.couleur =c;
		this.casePasse=0;
	}
	
	/**
	 * represente un cycliste avec sa couleur et son nombre de case parcourues
	 */
	public String toString(){
		return("" +this.couleur+ " a parcouru "+this.casePasse +" cases");
	}
	

	/**
	 * Methode faisant effectuer un mouvement pour le cyliste courant
	 * @param c carte qu'on joue
	 */
	public void fairemouvement(Carte c){
		//si jamais on est a une case superieur a 0 (on est pas sur la ligne de depart)
		if(this.caseTuile>0){
			//
			this.circuitCourant.tuileALaPosition(positionActuelle).partCases(this.caseTuile);
		}
		
		//on initialise deplace au numero de la carte (le mouvement)
		int deplace = c.getNumero();
		//verification pour la monte
		if(this.circuitCourant.tuileALaPosition(this.positionActuelle).estDescente() && deplace < 5){
			deplace = 5;
		}
		
		int nbCasesTuile = this.circuitCourant.tuileALaPosition(positionActuelle).getNbCases();
		//Le deplacement n'entraine pas un changement de tuile
		
		if((this.circuitCourant.getTabTuile().size()-1 == this.positionActuelle) && deplace+this.caseTuile > nbCasesTuile){
			deplace = nbCasesTuile-this.caseTuile;
			this.casePasse += deplace;
			this.verificationEtPlacage(deplace,nbCasesTuile);
			
			
		}else if(deplace + this.caseTuile <=nbCasesTuile){
			if(this.circuitCourant.tuileALaPosition(positionActuelle).estMonte() && deplace >5){
				deplace = 5;
			}
			this.verificationEtPlacage(deplace,nbCasesTuile);
			this.casePasse += deplace;
		
		//Le deplacement entraine un changement de tuile
		}else{
			boolean finForce = true;
			if(this.circuitCourant.tuileALaPosition(positionActuelle).estMonte() && deplace >5){
				deplace = 5;
			}
			int deplacementIni = deplace;
			int temp = 0;
			//Au nombbr de case en deplacement on ajoute la case sur laquelle est poisitonne le cycliste 4+3=7
			deplace +=this.caseTuile;
			//sa position actuelle = 0
			this.caseTuile=0;
			//Tant que le deplacement est superieur au nombre de cases de la tuile et que la condition d'arret n'est pas force
			while((deplace > nbCasesTuile) && finForce){
				//On enlève au deplacement le nombre de cases de la tuile 7-5 = 2
				deplace -= nbCasesTuile;
				//la var temporaire est incrementer du nb de cases et du deplacement =7+5=12
				temp += deplace+nbCasesTuile;
				this.positionActuelle++;
				if(this.positionActuelle==this.circuitCourant.getTabTuile().size()){
					this.positionActuelle--;
					this.caseTuile=this.circuitCourant.getTabTuile().get(this.circuitCourant.getTabTuile().size()-1).getNbCases();
				}
				nbCasesTuile = this.circuitCourant.tuileALaPosition(this.positionActuelle).getNbCases();
				if(this.circuitCourant.tuileALaPosition(this.positionActuelle).estMonte()){
					if(temp>=5){
						deplace = 1;
						finForce = false;
					}
					else{
						deplace = 5-temp;
						finForce = false;
					}
				}
			}
			this.casePasse += deplacementIni;
			this.verificationEtPlacage(deplace,nbCasesTuile);
		}
	}
	
	/**
	 * Permet de savoir le nombre de case de deplacement du cyciste
	 * @return le nombre de cases parcourues
	 */
	public int getCasePasse() {
		return casePasse;
	}

	/**
	 * Permet d'attribuer à un cycliste le nombre de cartes qu'il a passe 
	 * @param casePasse nouveau nombre de cases passees 
	 */
	public void setCasePasse(int casePasse) {
		this.casePasse = casePasse;
	}

	/**
	 * Verifie que la case sur la case va se deplacer n'est pas dejà occupee et sinon va le placer en consequence  
	 * @param deplacement nombre de case de deplacement à partir de la case actuelle du cycliste
	 * @param nbCasesTuile nombre de cases sur la tuile actuelle
	 */
	public void verificationEtPlacage(int deplacement,int nbCasesTuile){
			//on prend la prochaine case de tuile
			this.caseTuile+=deplacement;
			//tant qu'on ne peut y aller (deja 2 cyclistes sur la droit et la gauche de la case)
			while(!this.circuitCourant.tuileALaPosition(positionActuelle).peutAller(caseTuile)){
				
				this.casePasse--;
				this.caseTuile--;
				//si on change de tuile
				if(this.caseTuile<=0){
					this.positionActuelle--;
					nbCasesTuile = this.circuitCourant.getTabTuile().get(this.positionActuelle).getNbCases();
					this.caseTuile=nbCasesTuile;
				}
			}
			int nb = this.circuitCourant.tuileALaPosition(positionActuelle).setCase(this.caseTuile);
			if(nb==2){
				this.fileDroite = false;
			}
			else{
				this.fileDroite= true;
			}
	}
	

	/**
	 * Fais subir l'asbiration au cyslite courant
	 */
	public void subitAspiration(){
		this.casePasse++;
		if(this.caseTuile+1 > this.circuitCourant.getTabTuile().get(this.positionActuelle).getNbCases()){
			this.positionActuelle++;
			this.caseTuile=1;
		}
		else{
			this.caseTuile++;
		}
	}
	
	/**
	 * Permet de savoir si un cycliste a franchit la ligne
	 * @return vrai si le cycliste a franchi la ligne
	 */
	public boolean aPasseLaLigne(){
		return this.positionActuelle == (this.circuitCourant.getTabTuile().size()-1);
	}

	/**
	 * Permet de savoir sur quelle case de sa tuile le cycliste se trouve
	 * @return la case sur laquelle est le cycliste
	 */
	public int getCaseTuile() {
		return caseTuile;
	}

	/**
	 * Permet de definir le numero de la case sur laquelle se trouve le cycliste (pour les tests)
	 * @param caseTuile numero de la case sur laquelle on veut mettre le cycliste
	 */
	public void setCaseTuile(int caseTuile) {
		this.casePasse=caseTuile;
		this.caseTuile = caseTuile;
	}

	/**
	 * Permet d'avoir la couleur du cyliste
	 * @return couleur du cycliste
	 */
	public String getCouleur() {
		return couleur;
	}

	/**
	 * Permet de modifier la couleur du cycliste
	 * @param couleur nouvelle couleur du cycliste
	 */
	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}

	/**
	 * Permet de connaitre le numero de tuile sur lequel se trouve le cycliste
	 * @return numero de la tuile dans la liste de tuile
	 */
	public int getPositionActuelle() {
		return positionActuelle;
	}

	/**
	 * Permet de modifier le numero de tuile sur laquel se trouve le cycliste
	 * @param positionActuelle nouveau numero de tuile voulue pour le cycliste
	 */
	public void setPositionActuelle(int positionActuelle) {
		this.positionActuelle = positionActuelle;
	}

	/**
	 * Permet de savoir si un cycliste est sur la file de droite
	 * @return vrai si le cycliste est sur la file de droite
	 */
	public boolean isFileDroite() {
		return fileDroite;
	}

	/**
	 * Permet de mettre un cycliste sur la file de droite
	 * @param fileDroite nouvelle valeur de position
	 */
	public void setFileDroite(boolean fileDroite) {
		this.fileDroite = fileDroite;
	}

}
