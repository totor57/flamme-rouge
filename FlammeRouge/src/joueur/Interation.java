package joueur;
import circuit.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

import cartes.Carte;


/**
 * Classe d'interaction avec l'utilisateur en mode consol
 * @author totor & blondin
 *
 */
public abstract class Interation {
	/**
	 * Scanner des interactions
	 */
	public static Scanner sc =  new Scanner(System.in);
	
	
	
	/**
	 * Methode pour lire un nombre
	 * @return nombre lu
	 */
	public static int getNumber(){
		int x = sc.nextInt();
		return x;
	}
	
	
	/**
	 * Methode d'interaction au debut d'une partie
	 * @return choix de charger une sauvegarde ou non
	 */
	public static String main_debut_Jeu(){
		System.out.println("Bonjour et bienvenue dans le jeu de la flamme Rouge!");
		System.out.println("Souhaitez-vous charger une partie? Si oui tapez 1 sinon tapez autre chose!");
		String res = sc.nextLine();
		return res;
	}
	
	/**
	 * Methode lors d'un chargement de sauvegarde
	 * @return nom de la sauvegarde à charger
	 */
	public static String main_chargerSauvegarde(){
		System.out.println("Veuillez entrer le nom de la sauvegarde!");
		String res = sc.nextLine();
		return res;
	}
	
	/**
	 * Methode lors de la la fin d'un tour demandant si on veut faire une sauvegarde
	 * @return choix d'effectuer ou pas une sauvegarde
	 */
	public static String main_faireSauvegarde(){
		sc.nextLine();
		System.out.println("Si vous souhaitez faire une pause et sauvegarder la partie tapez 1!");
		String res = sc.next();
		return res;
	}
	
	
	/**
	 * Permet à l'utilisateur de creer un circuit
	 * @return circuit cree
	 */
	public static Circuit main_creerCircuit(){
		System.out.println("Afin de créer un circuit nous allons vous demander d'écrire sur une ligne le circuit que vous voulez");
		System.out.println("Veuillez respecter la syntaxe c'est à dire: \n départ=>0 \n ligneDroite=>1 \n virage séré droite=>2 \n virage légé droite=>3 \n arrivé=>4 \n virage serré gauche=>5 \n virage légé gauche =>6");
		String circ = sc.next();
		System.out.println(circ.charAt(circ.length()-1));
		while(circ.length()<3 || circ.charAt(0) != '0' || circ.charAt(circ.length()-1) != '4'){
			System.out.println("Il y a un souci dans votre circuit! \n (taille inférieur à 3 ou la première tuile n'est pas un départ ou la dernière n'est pas une arrivé");
			System.out.println("Veuillez rentrer une nouvelle composition");
			circ=sc.next();
		}
		System.out.println("Quel nom souhaitez vous donner à votre circuit?");
		String nom = sc.next();
		while(nom.length()<1){
			System.out.println("Entrez un nom non vide");
			nom = sc.next();
		}
		try{
			String path=new File("").getAbsolutePath();
			PrintWriter fichier = new PrintWriter(new FileWriter(path+"/src/circuitcarte/"+nom));
			fichier.println(circ);
			fichier.close();
		}
		catch(IOException e){
			System.out.println("Il y a eu un souci dans la création de votre circuit!");
		}
		Circuit res = null;
		try{
			res = Circuit.setCircuit(nom);
		}
		catch(IOException e){
			System.out.println("Il y a eu un soucis dans la création du circuit");
		}
		return res;
	}
	
	/**
	 * Methode lors de la sauvegarde pour entrer le nom
	 * @return Nom de la sauvegarde choisi
	 */
	public static String main_nomSauvegarde(){
		System.out.println("Veuillez entrer le nom de la sauvegarde (attention vous ecraserez une sauvegarde avec le même nom si elle existe!)");
		String res = sc.next();
		return res;
	}
	
	/**
	 * Methode pour choisir le nombres de joeurs
	 * @return nombre de joueurs pour la partie
	 */
	public static int main_entreJoueurs(){
		System.out.println("A combien souhaitez-vous jouer?");
		boolean continuer = false;
		int nbJ =-1;
		while(!continuer){
			try{
				nbJ = sc.nextInt();
				if(nbJ<2 || nbJ >4){
					System.out.println("Entrez un nombre de joueurs valide! (Entre 2 et 4) :");
				}
				else{
					continuer = true;
				}
			}
			catch(Exception e){
				System.out.println("Ce n'est pas un nombre ça! Veuillez re rentrer le nombre de joueurs");
				sc.next();
			}
		}
		System.out.println("Bien maintenant chaque joueur à son tour va entrer son pseudo et se voir attribuer une couleur!");
		return nbJ;
	}
	
	/**
	 * Methode d'affichage lors de la fin du main remerciant les joueurs
	 */
	public static void main_finMain(){
		System.out.println("Felicitation à tous les joueurs !");
		System.out.println("Le classement est disponible dans le fichier dernierClassement à la racine du jeu!");
		System.out.println("Merci d'avoir joue à bientôt :)");
	}
	
	/**
	 * Methode permettant d'entrer les noms de chaque joueur et de les associer à une couleur
	 * @param i indice du numero de joueur
	 * @param listeJ liste des joueurs du main
	 */
	public static void main_nomJoueur(int i,List<Joueur> listeJ,Circuit circCourant){
		if(i == 0){
			sc.nextLine();
		}
		String[] couleur = {"bleu","rouge","vert","jaune"};
		System.out.println("Joueur "+(i+1)+" entrez votre pseudo:");
		String nom = sc.next();
		while(nom.length()==0){
			System.out.println("Votre pseudo ne peut pas être rien ;) Entrez de nouveau votre pseudo!");
			nom = sc.next();
		}
		System.out.println("Enchante "+nom+" vous êtes la couleur "+couleur[i]);
		Joueur temp = new Joueur(nom,couleur[i],circCourant);
		listeJ.add(temp);
	}
	
	
	/**
	 * Methode permettant de gerer le fait que deux cyclistes est passes la ligne d'arrive en meme temps (on fait un jeu du + ou -)
	 * @param a Cycliste a
	 * @param b Cycliste b
	 * @return Cycliste qui gagne le jeu!
	 */
	public static Cycliste departager_deuxCyclistes(Cycliste a,Cycliste b){
		System.out.println("Il semblerait que le joueur " +a.getCouleur() + " et le joueur "+b.getCouleur()+" soit à egalite!");
		System.out.println("pour les departager nous allons jouer à un petit jeu!!");
		boolean gagnant = false;
		Cycliste res = null;
		while(!gagnant){
			System.out.println(a.getCouleur()+" choisi un nombre entre 0 et 100");
			int ca = sc.nextInt();
			while(ca <0 || ca >100){
				System.out.println(a.getCouleur()+" choisi un nombre entre 0 et 100 et pas autre chose!");
				ca = sc.nextInt();
			}
			System.out.println(a.getCouleur()+" choisi un nombre entre 0 et 100");
			int cb = sc.nextInt();
			while(cb < 0 || cb > 100){
				System.out.println(a.getCouleur()+" choisi un nombre entre 0 et 100");
				cb = sc.nextInt();
			}
			int val = (int)(Math.random()*100);
			if(Math.abs(val-ca) < Math.abs(val-cb)){
				res=a;
				gagnant=true;
			}
			else if(Math.abs(val-ca) > Math.abs(val-cb)){
				res=b;
				gagnant=true;
			}
			else{
				System.out.println("Decidement encore egalite! Veuillez refaire le jeu SVP!");
			}
		}
		return res;
	}
	
	/**
	 * Methode d'interaction au debut du jeu courant
	 */
	public static void jeu_PosDepartDebut(){
		System.out.println("Bien maintenant c'est le moment de placer vos cyclistes! Comme je ne peux connaitre vos âges ce sera à tour de rôle à partir du premier joueur");
		System.out.println("Pour choisir votre position de depart vous allez entrez un chiffres entre 0 et 3, 0 etant sur la ligne d'arrive et 3 au plus loin");
		System.out.println("Par default vous serez sur la ligne de droite, si un autre cycliste choisis la même position il sera à gauche, les autres devronts trouver une autre place!");
	}
	
	/**
	 * Permet de mettre un message adapte en fonction du joueur pour le choix de son rouleur
	 * @param n nom du joueur
	 */
	public static void jeu_PosDepartMilieu(String n){
		System.out.println("C'est à " +n);
		System.out.println("Choisis une position pour ton rouleur");
	}
	
	/**
	 * Permet de mettre un message adapte en fonction du joueur pour le choix de son sprinteur
	 */
	public static void jeu_PosDepartMilieu2(){
		System.out.println("Choisis une position pour ton sprinteur");
	}
	
	
	/**
	 * Message si la case est deja prise lors du placement d'un cycliste 
	 */
	public static void jeu_PosDepartBoucle(){
		System.out.println("Cette place n'est pas libre ! Choisis une position libre pour ton sprinteur");
	}
	
	/**
	 * Permet d'avoir un message adapte en fonction du nom du joueur
	 * @param n nom du joueur
	 */
	public static void jeu_Piocher(String n){
		System.out.println(n + " c'est à toi de piocher!");
	}
	
	/**
	 * Demande a un joueur si il veux d'abord piocher une carte pour son sprinteur ou pour son rouleur
	 * @return choix de pioche du joueur
	 */
	public static String joueur_piocher1(){
		System.out.println("Si vous voulez d'abord piocher pour votre sprinteur: tapez 1 sinon tapez n'importe quel valeur :");
		sc.nextLine();
		String n = sc.nextLine();
		return n;
	}
	
	/**
	 * Message indiquant si les 4 cartes son pour rouleur ou sprinteur
	 * @param choixCoureur choix de jeu du joueur
	 */
	public static void joueur_choisir3cartes(int choixCoureur){
		if(choixCoureur==1){
			System.out.println("Voici les 3 cartes pour ton sprinteur:");
		}
		else{
			System.out.println("Voici les 3 cartes pour ton rouleur:");
		}
	}
	
	/**
	 * Permet d'afficher les cartes que le joueur a pioch
	 * @param i numero de la carte parmis celles qu'il a pioche
	 * @param c carte courante
	 */
	public static void joueur_choisir3cartes2(int i,Carte c){
		System.out.println("Carte numero "+(i+1)+" qui est une carte energie "+ c);
	}
	
	/**
	 * Permet de choisir parmi les 3 cartes pioche
	 * @return numero de la carte choisi
	 */
	public static int joueur_choisir3cartes3(){
		System.out.println("Tapez le numero correspondant à la carte que vous voulez jouer pour le prochain tour pour ce coureur!");
		int x = 0;
		boolean valide =false;
		while(!valide){
			try{
				x=sc.nextInt();
				if((x<1||x>3)){
					System.out.println("Non valide! Mettez un autre numero");
				}
				else{
					valide = true;
				}
			}
			catch(Exception e){
				System.out.println("Ce n'est pas un nombre ca! Entrez un nouveau numéro");
				debug();
			}
		}
		return x;
	}
	
	/**
	 * Methode permettant de demander si le joueur veut une etape de montage
	 * @return choix du joueur
	 */
	public static String circuit_montagne(){
		System.out.println("Souhaitez vous faire une étape de montage? (1 pour oui) sinon tapez n'importe quoi d'autres");
		String res = sc.nextLine();
		return res;
	}
	
	/**
	 * Debug utilise en cas de saut du scanner 
	 */
	public static void debug(){
		sc.next();
	}
	
}
